/* eeprom.c */

#include <avr/io.h>
#include <stdbool.h>

void EEPROM_write(unsigned int uiAddress, unsigned char ucData)
{
    /* Wait for completion of previous write */
    while(EECR & (1<<EEPE));
    /* Set up address and data registers */
    EEAR = uiAddress;
    EEDR = ucData;
    /* Write logical one to EEMWE */
    EECR |= (1<<EEMPE);
    /* Start eeprom write by setting EEWE */
    EECR |= (1<<EEPE);
}

void EEPROM_write_uint(unsigned int uiAddress, unsigned int uiData)
{
    EEPROM_write(uiAddress, uiData);
    EEPROM_write(uiAddress + 1, uiData >> 8);
}

unsigned char EEPROM_read(unsigned int uiAddress)
{
    /* Wait for completion of previous write */
    while(EECR & (1<<EEPE));
    /* Set up address register */
    EEAR = uiAddress;
    /* Start eeprom read by writing EERE */
    EECR |= (1<<EERE);
    /* Return data from data register */
    return EEDR;
}

unsigned int EEPROM_read_uint(unsigned int uiAddress)
{
    unsigned int data = EEPROM_read(uiAddress);
    data = data | (EEPROM_read(uiAddress + 1) << 8);
    return data;
}

bool EEPROM_write_str(unsigned int uiAddress, const char *str, int eeMemLgt)
{
    int charCount = 0;

    while ((str[charCount] != 0) && (charCount < eeMemLgt))
    {
        EEPROM_write(uiAddress + charCount, (unsigned char)str[charCount]);
        charCount++;
    }

    if (charCount < eeMemLgt)
    {
        EEPROM_write(uiAddress + charCount, 0);
        return true;
    }

    return false;
}

bool EEPROM_read_str(unsigned int uiAddress, char *str, int eeMemLgt)
{
    int charCount = 0;

    while (charCount < eeMemLgt)
    {
        unsigned char readChar = EEPROM_read(uiAddress + charCount);
        if (readChar == 0)
        {
            str[charCount] = 0;
            return true;
        }

        str[charCount] = readChar;
        charCount++;
    }

    return false;
}
