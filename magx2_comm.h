//******************************************************************************
//  Komunikace s MagX2 - modul je v pozici mastera na seriove sbernici.
//*****************************************************************************/

#ifndef MAGX2_COMM_H_INCLUDED
#define MAGX2_COMM_H_INCLUDED

// Vysledek komunikace
typedef enum
{
    CRES_OK, // bez chyb
    CRES_TMR, // timeout
    CRES_ERR // chyba

} TCommResult;

// Data komunikace
typedef struct
{
    // vysledek komunikace
    TCommResult commResult;

    // bufer
    unsigned char *buffer;

    // pocet bytu v buferu
    unsigned int bufferSize;

} TCommData;

// Typ - prototyp funkce, ktera je volana po prijmu odpovedi na dotaz.
typedef void (*TMagX2CommDataHandler) (const TCommData *commData);

// zadna funkce po prijmu odpovedi
#define NO_MAGX2_COMM_DATA_HANDLER   ((TMagX2CommDataHandler)0)

// datum a cas
typedef struct
{
    unsigned long date;
    unsigned long time;

} TDateTime;

// casti totalizeru
typedef struct
{
    unsigned long dig; // cela
    unsigned long dec; // desetinna

} TTotalParts;

// casti prutoku
typedef struct
{
    unsigned long abs; // abs. prutok * 1000
    unsigned char sign; // znamenko (0 je +, ostatni minus)

} TFlowParts;

//------------------------------------------------------------------------------
//  Inicializace rozhrani, volat jenom jednou pred ostatnimi funkcemi rozhrani.
//------------------------------------------------------------------------------
void MagX2Comm_Init(void);

//------------------------------------------------------------------------------
//  Hlavni uloha, volat pravidelne a co nejcasteji.
//------------------------------------------------------------------------------
void MagX2Comm_Task(void);

//------------------------------------------------------------------------------
//  Stop prijmu.
//------------------------------------------------------------------------------
void MagX2Comm_StopReceiving(void);

//------------------------------------------------------------------------------
//  Zpracuje prijata data
//------------------------------------------------------------------------------
void MagX2Comm_ProcessRcvData(const char *rcvBuffer, unsigned int rcvBufferByteCount);

//------------------------------------------------------------------------------
//  Parsing typu unsigned long MagX2.
//------------------------------------------------------------------------------
unsigned long MagX2Comm_ParseUlong(const unsigned char *buffer);

//------------------------------------------------------------------------------
//  Zapis hesel MagX2.
//------------------------------------------------------------------------------
void MagX2Comm_WritePasswords(TMagX2CommDataHandler handler);

//------------------------------------------------------------------------------
//  Cteni cisla MagX2.
//------------------------------------------------------------------------------
void MagX2Comm_ReadUnitNo(TMagX2CommDataHandler handler);

//------------------------------------------------------------------------------
//  Parsing cisla MagX2.
//------------------------------------------------------------------------------
unsigned long MagX2Comm_ParseUnitNo(const unsigned char *buffer);

//------------------------------------------------------------------------------
//  Cteni Error min MagX2.
//------------------------------------------------------------------------------
void MagX2Comm_ReadErrorMin(TMagX2CommDataHandler handler);

//------------------------------------------------------------------------------
//  Parsing Error min MagX2.
//------------------------------------------------------------------------------
unsigned long MagX2Comm_ParseErrorMin(const unsigned char *buffer);

//------------------------------------------------------------------------------
//  Cteni Diameter MagX2.
//------------------------------------------------------------------------------
void MagX2Comm_ReadDiameter(TMagX2CommDataHandler handler);

//------------------------------------------------------------------------------
//  Parsing Diameter MagX2.
//------------------------------------------------------------------------------
unsigned long MagX2Comm_ParseDiameter(const unsigned char *buffer);

//------------------------------------------------------------------------------
//  Cteni actual error MagX2.
//------------------------------------------------------------------------------
void MagX2Comm_ReadActualError(TMagX2CommDataHandler handler);

//------------------------------------------------------------------------------
//  Parsing actual error MagX2.
//------------------------------------------------------------------------------
unsigned long MagX2Comm_ParseActualError(const unsigned char *buffer);

//------------------------------------------------------------------------------
//  Cteni datumu MagX2.
//------------------------------------------------------------------------------
void MagX2Comm_ReadDate(TMagX2CommDataHandler handler);

//------------------------------------------------------------------------------
//  Parsing datumu.
//------------------------------------------------------------------------------
void MagX2Comm_ParseDate(const unsigned char *buffer, TDateTime *dateTime);

//------------------------------------------------------------------------------
//  Cteni casu MagX2.
//------------------------------------------------------------------------------
void MagX2Comm_ReadTime(TMagX2CommDataHandler handler);

//------------------------------------------------------------------------------
//  Parsing casu.
//------------------------------------------------------------------------------
void MagX2Comm_ParseTime(const unsigned char *buffer, TDateTime *dateTime);

//------------------------------------------------------------------------------
//  Cteni casti totalizeru MagX2.
//------------------------------------------------------------------------------
void MagX2Comm_ReadTotalParts(TMagX2CommDataHandler handler);

//------------------------------------------------------------------------------
//  Parsing casti totalizeru.
//------------------------------------------------------------------------------
void MagX2Comm_ParseTotalParts(const unsigned char *buffer, TTotalParts *totalParts);

//------------------------------------------------------------------------------
//  Cteni casti prutoku MagX2.
//------------------------------------------------------------------------------
void MagX2Comm_ReadFlowParts(TMagX2CommDataHandler handler);

//------------------------------------------------------------------------------
//  Parsing casti prutoku.
//------------------------------------------------------------------------------
void MagX2Comm_ParseFlowParts(const unsigned char *buffer, TFlowParts *flowParts);

//------------------------------------------------------------------------------
//  Cteni prutoku MagX2.
//------------------------------------------------------------------------------
void MagX2Comm_ReadFlowFloat(TMagX2CommDataHandler handler);

//------------------------------------------------------------------------------
//  Parsing prutoku.
//------------------------------------------------------------------------------
float MagX2Comm_ParseFlowFloat(const unsigned char *buffer);

//------------------------------------------------------------------------------
//  Cteni Total MagX2.
//------------------------------------------------------------------------------
void MagX2Comm_ReadTotalFloat(TMagX2CommDataHandler handler);

//------------------------------------------------------------------------------
//  Parsing Total.
//------------------------------------------------------------------------------
float MagX2Comm_ParseTotalFloat(const unsigned char *buffer);

//------------------------------------------------------------------------------
//  Cteni Aux MagX2.
//------------------------------------------------------------------------------
void MagX2Comm_ReadAuxFloat(TMagX2CommDataHandler handler);

//------------------------------------------------------------------------------
//  Parsing Aux.
//------------------------------------------------------------------------------
float MagX2Comm_ParseAuxFloat(const unsigned char *buffer);

//------------------------------------------------------------------------------
//  Cteni Total+ MagX2.
//------------------------------------------------------------------------------
void MagX2Comm_ReadTotalPlusFloat(TMagX2CommDataHandler handler);

//------------------------------------------------------------------------------
//  Parsing Total+.
//------------------------------------------------------------------------------
float MagX2Comm_ParseTotalPlusFloat(const unsigned char *buffer);

//------------------------------------------------------------------------------
//  Cteni Total- MagX2.
//------------------------------------------------------------------------------
void MagX2Comm_ReadTotalMinusFloat(TMagX2CommDataHandler handler);

//------------------------------------------------------------------------------
//  Parsing Total-.
//------------------------------------------------------------------------------
float MagX2Comm_ParseTotalMinusFloat(const unsigned char *buffer);

//------------------------------------------------------------------------------
//  Cteni teploty MagX2.
//------------------------------------------------------------------------------
void MagX2Comm_ReadTemperatureFloat(TMagX2CommDataHandler handler);

//------------------------------------------------------------------------------
//  Parsing teploty.
//------------------------------------------------------------------------------
float MagX2Comm_ParseTemperatureFloat(const unsigned char *buffer);

//------------------------------------------------------------------------------
//  Cteni externi teploty MagX2.
//------------------------------------------------------------------------------
void MagX2Comm_ReadExtTemperatureFloat(TMagX2CommDataHandler handler);

//------------------------------------------------------------------------------
//  Parsing externi teploty.
//------------------------------------------------------------------------------
float MagX2Comm_ParseExtTemperatureFloat(const unsigned char *buffer);

//------------------------------------------------------------------------------
//  Cteni externiho tlaku MagX2.
//------------------------------------------------------------------------------
void MagX2Comm_ReadExtPressureFloat(TMagX2CommDataHandler handler);

//------------------------------------------------------------------------------
//  Parsing externiho tlaku.
//------------------------------------------------------------------------------
float MagX2Comm_ParseExtPressureFloat(const unsigned char *buffer);

#endif // MAGX2_COMM_H_INCLUDED
