//******************************************************************************
//  Systemovy tik - 1ms
//*****************************************************************************/

#ifndef SYS_TICK_H_INCLUDED
#define SYS_TICK_H_INCLUDED

//------------------------------------------------------------------------------
//  Inicializace rozhrani, volat jenom jednou pred ostatnimi funkcemi rozhrani.
//------------------------------------------------------------------------------
void SysTick_Init(void);

//------------------------------------------------------------------------------
//  Vrati stav systemovych milisekund.
//------------------------------------------------------------------------------
unsigned long SysTick_GetSysMSeconds(void);

//------------------------------------------------------------------------------
//  Zjisti, zda vyprsela prodleva v msec.
//------------------------------------------------------------------------------
unsigned char SysTick_IsSysMSecondTimeout(unsigned long userSysMSeconds, unsigned long delayMSeconds);

#endif // SYS_TICK_H_INCLUDED
