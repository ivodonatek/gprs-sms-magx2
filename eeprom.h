/* eeprom.h */

#ifndef eeprom
#define eeprom

extern void EEPROM_write(unsigned int uiAddress, unsigned char ucData);
extern void EEPROM_write_uint(unsigned int uiAddress, unsigned int uiData);
extern unsigned char EEPROM_read(unsigned int uiAddress);
extern unsigned int EEPROM_read_uint(unsigned int uiAddress);
extern bool EEPROM_write_str(unsigned int uiAddress, const char *str, int eeMemLgt);
extern bool EEPROM_read_str(unsigned int uiAddress, char *str, int eeMemLgt);

#endif
