//******************************************************************************
//  Komunikace s MagX2 - modul je v pozici mastera na seriove sbernici.
//*****************************************************************************/

#include <stdbool.h>
#include <string.h>
#include "magx2_comm.h"
#include "sys_tick.h"
#include "usart.h"

///slave adresa magx2
#define SLAVE_ID 0x01

///function
#define READ_HOLDING_REGISTER       0x03
#define WRITE_MULTIPLE_REGISTER     0x10

#define RCV_TIMEOUT         2000      //timeout prijmu [ms]

///modbus adresy
#define MAGX2_MODBUS_PASSWORD_USER                  (2-1)
#define MAGX2_MODBUS_PASSWORD_SERVICE               (4-1)
#define MAGX2_MODBUS_PASSWORD_FACTORY               (6-1)
#define MAGX2_MODBUS_MEAS_TOTAL_DIG                 (102-1)
#define MAGX2_MODBUS_MEAS_TOTAL_DEC                 (104-1)
#define MAGX2_MODBUS_MEAS_FLOW_ABS                  (126-1)
#define MAGX2_MODBUS_MEAS_FLOW_SIGN                 (128-1)
#define MAGX2_MODBUS_MEAS_FLOW_FLOAT                (150-1)
#define MAGX2_MODBUS_MEAS_TOTAL_FLOAT               (152-1)
#define MAGX2_MODBUS_MEAS_AUX_FLOAT                 (154-1)
#define MAGX2_MODBUS_MEAS_POS_FLOAT                 (156-1)
#define MAGX2_MODBUS_MEAS_NEG_FLOAT                 (158-1)
#define MAGX2_MODBUS_MEAS_TEMP_FLOAT                (160-1)
#define MAGX2_MODBUS_EXT_MEAS_TEMP_FLOAT            (162-1)
#define MAGX2_MODBUS_EXT_MEAS_PRESS_FLOAT           (164-1)
#define MAGX2_MODBUS_UNIT_NO                        (1000-1)
#define MAGX2_MODBUS_ERROR_MIN                      (1004-1)
#define MAGX2_MODBUS_DIAMETER                       (1008-1)
#define MAGX2_MODBUS_ACTUAL_ERROR                   (1014-1)
#define MAGX2_MODBUS_DATE                           (3034-1)
#define MAGX2_MODBUS_TIME                           (3036-1)

/* These types are assumed as 8-bit integer */
typedef signed char		CHAR;
typedef unsigned char	UCHAR;
#ifndef BYTE_TYPEDEF
#define BYTE_TYPEDEFED
typedef unsigned char	BYTE;
#endif

/* These types are assumed as 16-bit integer */
typedef signed short	SHORT;
typedef unsigned short	USHORT;
#ifndef WORD_TYPEDEF
#define WORD_TYPEDEF
typedef unsigned short	WORD;
#endif

// stav komunikace
typedef enum
{
    CMSTA_NONE, // nic se nedeje
    CMSTA_TX, // vysilani
    CMSTA_RX // prijem

} TCommStatus;

// Fw module state
static struct
{
    // stav komunikace
    TCommStatus commStatus;

    // prijimaci casovac
    unsigned long rcvTimer;

    // funkce, ktera je volana po prijmu odpovedi na dotaz
    TMagX2CommDataHandler magX2CommDataHandler;

} state;

//------------------------------------------------------------------------------
//  Prevod ulong do float
//------------------------------------------------------------------------------
static float UlongToFloat(unsigned long value)
{
    float result;
    memcpy(&result, &value, 4);
    return result;
}

//------------------------------------------------------------------------------
//  Start prijimaciho casovace
//------------------------------------------------------------------------------
static void StartRcvTimer(void)
{
    state.rcvTimer = SysTick_GetSysMSeconds();
}

//------------------------------------------------------------------------------
//  Zjisteni prijimaciho timeoutu
//------------------------------------------------------------------------------
static unsigned char IsRcvTimeout(void)
{
    return SysTick_IsSysMSecondTimeout(state.rcvTimer, RCV_TIMEOUT);
}

//------------------------------------------------------------------------------
//  Zpracuje timeout prijmu
//------------------------------------------------------------------------------
static void ProcessRcvTimeout(void)
{
    state.commStatus = CMSTA_NONE;
    if (state.magX2CommDataHandler != NO_MAGX2_COMM_DATA_HANDLER)
    {
        TCommData commData;
        commData.commResult = CRES_TMR;
        commData.buffer = (unsigned char *)0;
        commData.bufferSize = 0;

        state.magX2CommDataHandler(&commData);
    }
}

//------------------------------------------------------------------------------
//  Odeslani dat na sbernici
//------------------------------------------------------------------------------
static void SendData(const unsigned char *dataBuffer, unsigned int dataCount, TMagX2CommDataHandler handler)
{
    state.commStatus = CMSTA_TX;

    mcu_send_data((unsigned char *)dataBuffer, dataCount);

    state.commStatus = CMSTA_RX;
    state.magX2CommDataHandler = handler;

    StartRcvTimer();
}

static const UCHAR aucCRCHi[] = {
    0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41,
    0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
    0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41,
    0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
    0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41,
    0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
    0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
    0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
    0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41,
    0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
    0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41,
    0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
    0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41,
    0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
    0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
    0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
    0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41,
    0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
    0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41,
    0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
    0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41,
    0x00, 0xC1, 0x81, 0x40
};

static const UCHAR aucCRCLo[] = {
    0x00, 0xC0, 0xC1, 0x01, 0xC3, 0x03, 0x02, 0xC2, 0xC6, 0x06, 0x07, 0xC7,
    0x05, 0xC5, 0xC4, 0x04, 0xCC, 0x0C, 0x0D, 0xCD, 0x0F, 0xCF, 0xCE, 0x0E,
    0x0A, 0xCA, 0xCB, 0x0B, 0xC9, 0x09, 0x08, 0xC8, 0xD8, 0x18, 0x19, 0xD9,
    0x1B, 0xDB, 0xDA, 0x1A, 0x1E, 0xDE, 0xDF, 0x1F, 0xDD, 0x1D, 0x1C, 0xDC,
    0x14, 0xD4, 0xD5, 0x15, 0xD7, 0x17, 0x16, 0xD6, 0xD2, 0x12, 0x13, 0xD3,
    0x11, 0xD1, 0xD0, 0x10, 0xF0, 0x30, 0x31, 0xF1, 0x33, 0xF3, 0xF2, 0x32,
    0x36, 0xF6, 0xF7, 0x37, 0xF5, 0x35, 0x34, 0xF4, 0x3C, 0xFC, 0xFD, 0x3D,
    0xFF, 0x3F, 0x3E, 0xFE, 0xFA, 0x3A, 0x3B, 0xFB, 0x39, 0xF9, 0xF8, 0x38,
    0x28, 0xE8, 0xE9, 0x29, 0xEB, 0x2B, 0x2A, 0xEA, 0xEE, 0x2E, 0x2F, 0xEF,
    0x2D, 0xED, 0xEC, 0x2C, 0xE4, 0x24, 0x25, 0xE5, 0x27, 0xE7, 0xE6, 0x26,
    0x22, 0xE2, 0xE3, 0x23, 0xE1, 0x21, 0x20, 0xE0, 0xA0, 0x60, 0x61, 0xA1,
    0x63, 0xA3, 0xA2, 0x62, 0x66, 0xA6, 0xA7, 0x67, 0xA5, 0x65, 0x64, 0xA4,
    0x6C, 0xAC, 0xAD, 0x6D, 0xAF, 0x6F, 0x6E, 0xAE, 0xAA, 0x6A, 0x6B, 0xAB,
    0x69, 0xA9, 0xA8, 0x68, 0x78, 0xB8, 0xB9, 0x79, 0xBB, 0x7B, 0x7A, 0xBA,
    0xBE, 0x7E, 0x7F, 0xBF, 0x7D, 0xBD, 0xBC, 0x7C, 0xB4, 0x74, 0x75, 0xB5,
    0x77, 0xB7, 0xB6, 0x76, 0x72, 0xB2, 0xB3, 0x73, 0xB1, 0x71, 0x70, 0xB0,
    0x50, 0x90, 0x91, 0x51, 0x93, 0x53, 0x52, 0x92, 0x96, 0x56, 0x57, 0x97,
    0x55, 0x95, 0x94, 0x54, 0x9C, 0x5C, 0x5D, 0x9D, 0x5F, 0x9F, 0x9E, 0x5E,
    0x5A, 0x9A, 0x9B, 0x5B, 0x99, 0x59, 0x58, 0x98, 0x88, 0x48, 0x49, 0x89,
    0x4B, 0x8B, 0x8A, 0x4A, 0x4E, 0x8E, 0x8F, 0x4F, 0x8D, 0x4D, 0x4C, 0x8C,
    0x44, 0x84, 0x85, 0x45, 0x87, 0x47, 0x46, 0x86, 0x82, 0x42, 0x43, 0x83,
    0x41, 0x81, 0x80, 0x40
};

static USHORT usMBCRC16( UCHAR * pucFrame, USHORT usLen )
{
    UCHAR           ucCRCHi = 0xFF;
    UCHAR           ucCRCLo = 0xFF;
    int             iIndex;

    while( usLen-- )
    {
        iIndex = ucCRCLo ^ *( pucFrame++ );
        ucCRCLo = ( UCHAR )( ucCRCHi ^ aucCRCHi[iIndex] );
        ucCRCHi = aucCRCLo[iIndex];
    }
    return ( USHORT )( ucCRCHi << 8 | ucCRCLo );
}

//------------------------------------------------------------------------------
//  Cteni holding DWORDu MagX2.
//------------------------------------------------------------------------------
static void ReadHoldingDWord(unsigned int address, TMagX2CommDataHandler handler)
{
    unsigned char buff[8];

    buff[0]=SLAVE_ID;
    buff[1]=READ_HOLDING_REGISTER;

    buff[2]=(unsigned char)(address >> 8);
    buff[3]=(unsigned char)(address);

    buff[4]=0x00;
    buff[5]=0x02;       //zadost musi byt delitelna dvema

    unsigned int crcecko=usMBCRC16(buff,6);   //kontrolni soucet
    buff[6]=(unsigned char)(crcecko);
    buff[7]=(unsigned char)(crcecko>>8);

    SendData(buff, 8, handler);
}

//------------------------------------------------------------------------------
//  Inicializace rozhrani, volat jenom jednou pred ostatnimi funkcemi rozhrani.
//------------------------------------------------------------------------------
void MagX2Comm_Init(void)
{
    state.commStatus = CMSTA_NONE;
    state.magX2CommDataHandler = NO_MAGX2_COMM_DATA_HANDLER;
}

//------------------------------------------------------------------------------
//  Hlavni uloha, volat pravidelne a co nejcasteji.
//------------------------------------------------------------------------------
void MagX2Comm_Task(void)
{
    if (state.commStatus == CMSTA_RX)
    {
        if (IsRcvTimeout())
            ProcessRcvTimeout();
    }
}

//------------------------------------------------------------------------------
//  Stop prijmu.
//------------------------------------------------------------------------------
void MagX2Comm_StopReceiving(void)
{
    state.commStatus = CMSTA_NONE;
}

//------------------------------------------------------------------------------
//  Zpracuje prijata data
//------------------------------------------------------------------------------
void MagX2Comm_ProcessRcvData(const char *rcvBuffer, unsigned int rcvBufferByteCount)
{
    if (state.commStatus != CMSTA_RX)
        return;

    if (rcvBufferByteCount < 3)
        return;

    switch (rcvBuffer[1])
    {
        case WRITE_MULTIPLE_REGISTER:
            if (rcvBufferByteCount >= 8)
            {
                state.commStatus = CMSTA_NONE;
                if (state.magX2CommDataHandler != NO_MAGX2_COMM_DATA_HANDLER)
                {
                    TCommData commData;
                    commData.commResult = CRES_OK;
                    commData.buffer = (unsigned char *)0;
                    commData.bufferSize = 0;

                    state.magX2CommDataHandler(&commData);
                }
            }
            break;

        case READ_HOLDING_REGISTER:
            if (rcvBufferByteCount == (rcvBuffer[2] + 5))
            {
                state.commStatus = CMSTA_NONE;
                if (state.magX2CommDataHandler != NO_MAGX2_COMM_DATA_HANDLER)
                {
                    TCommData commData;
                    commData.commResult = CRES_OK;
                    commData.buffer = (unsigned char *)&rcvBuffer[3];
                    commData.bufferSize = rcvBuffer[2];

                    state.magX2CommDataHandler(&commData);
                }
            }
            break;

        default:
            break;
    }
}

//------------------------------------------------------------------------------
//  Parsing typu unsigned long MagX2.
//------------------------------------------------------------------------------
unsigned long MagX2Comm_ParseUlong(const unsigned char *buffer)
{
    unsigned char pos=0;
    unsigned long help=0;

    help=(unsigned long)buffer[pos++]<<8;
    help+=(unsigned long)buffer[pos++];
    help+=(unsigned long)buffer[pos++]<<24;
    help+=(unsigned long)buffer[pos++]<<16;

    return help;
}

//------------------------------------------------------------------------------
//  Zapis hesel MagX2.
//------------------------------------------------------------------------------
void MagX2Comm_WritePasswords(TMagX2CommDataHandler handler)
{
    unsigned char buff[13];

    buff[0]=SLAVE_ID;
    buff[1]=WRITE_MULTIPLE_REGISTER;

    buff[2]=(unsigned char)(MAGX2_MODBUS_PASSWORD_SERVICE >> 8);
    buff[3]=(unsigned char)(MAGX2_MODBUS_PASSWORD_SERVICE);

    buff[4]=0x00;   //pocet registru
    buff[5]=2;

    buff[6]=4;   //pocet bytu

    unsigned long heslo=242628;        //service

    buff[7]=(unsigned char)(heslo >> 8);
    buff[8]=(unsigned char)(heslo);
    buff[9]=(unsigned char)(heslo >> 24);
    buff[10]=(unsigned char)(heslo >> 16);

    unsigned int crcecko=usMBCRC16(buff,11);;   //kontrolni soucet
    buff[11]=(unsigned char)(crcecko);
    buff[12]=(unsigned char)(crcecko>>8);

    SendData(buff, 13, handler);
}

//------------------------------------------------------------------------------
//  Cteni cisla MagX2.
//------------------------------------------------------------------------------
void MagX2Comm_ReadUnitNo(TMagX2CommDataHandler handler)
{
    ReadHoldingDWord(MAGX2_MODBUS_UNIT_NO, handler);
}

//------------------------------------------------------------------------------
//  Parsing cisla MagX2.
//------------------------------------------------------------------------------
unsigned long MagX2Comm_ParseUnitNo(const unsigned char *buffer)
{
    return MagX2Comm_ParseUlong(buffer);
}

//------------------------------------------------------------------------------
//  Cteni Error min MagX2.
//------------------------------------------------------------------------------
void MagX2Comm_ReadErrorMin(TMagX2CommDataHandler handler)
{
    ReadHoldingDWord(MAGX2_MODBUS_ERROR_MIN, handler);
}

//------------------------------------------------------------------------------
//  Parsing Error min MagX2.
//------------------------------------------------------------------------------
unsigned long MagX2Comm_ParseErrorMin(const unsigned char *buffer)
{
    return MagX2Comm_ParseUlong(buffer);
}

//------------------------------------------------------------------------------
//  Cteni Diameter MagX2.
//------------------------------------------------------------------------------
void MagX2Comm_ReadDiameter(TMagX2CommDataHandler handler)
{
    ReadHoldingDWord(MAGX2_MODBUS_DIAMETER, handler);
}

//------------------------------------------------------------------------------
//  Parsing Diameter MagX2.
//------------------------------------------------------------------------------
unsigned long MagX2Comm_ParseDiameter(const unsigned char *buffer)
{
    return MagX2Comm_ParseUlong(buffer);
}

//------------------------------------------------------------------------------
//  Cteni actual error MagX2.
//------------------------------------------------------------------------------
void MagX2Comm_ReadActualError(TMagX2CommDataHandler handler)
{
    ReadHoldingDWord(MAGX2_MODBUS_ACTUAL_ERROR, handler);
}

//------------------------------------------------------------------------------
//  Parsing actual error MagX2.
//------------------------------------------------------------------------------
unsigned long MagX2Comm_ParseActualError(const unsigned char *buffer)
{
    return MagX2Comm_ParseUlong(buffer);
}

//------------------------------------------------------------------------------
//  Cteni datumu MagX2.
//------------------------------------------------------------------------------
void MagX2Comm_ReadDate(TMagX2CommDataHandler handler)
{
    ReadHoldingDWord(MAGX2_MODBUS_DATE, handler);
}

//------------------------------------------------------------------------------
//  Parsing datumu.
//------------------------------------------------------------------------------
void MagX2Comm_ParseDate(const unsigned char *buffer, TDateTime *dateTime)
{
    dateTime->date = MagX2Comm_ParseUlong(buffer);;
}

//------------------------------------------------------------------------------
//  Cteni casu MagX2.
//------------------------------------------------------------------------------
void MagX2Comm_ReadTime(TMagX2CommDataHandler handler)
{
    ReadHoldingDWord(MAGX2_MODBUS_TIME, handler);
}

//------------------------------------------------------------------------------
//  Parsing casu.
//------------------------------------------------------------------------------
void MagX2Comm_ParseTime(const unsigned char *buffer, TDateTime *dateTime)
{
    dateTime->time = MagX2Comm_ParseUlong(buffer);;
}

//------------------------------------------------------------------------------
//  Cteni casti totalizeru MagX2.
//------------------------------------------------------------------------------
void MagX2Comm_ReadTotalParts(TMagX2CommDataHandler handler)
{
    unsigned char buff[8];

    buff[0]=SLAVE_ID;
    buff[1]=READ_HOLDING_REGISTER;

    buff[2]=(unsigned char)(MAGX2_MODBUS_MEAS_TOTAL_DIG >> 8);
    buff[3]=(unsigned char)(MAGX2_MODBUS_MEAS_TOTAL_DIG);

    buff[4]=0x00;
    buff[5]=4;

    unsigned int crcecko=usMBCRC16(buff,6);   //kontrolni soucet
    buff[6]=(unsigned char)(crcecko);
    buff[7]=(unsigned char)(crcecko>>8);

    SendData(buff, 8, handler);
}

//------------------------------------------------------------------------------
//  Parsing casti totalizeru.
//------------------------------------------------------------------------------
void MagX2Comm_ParseTotalParts(const unsigned char *buffer, TTotalParts *totalParts)
{
    unsigned long pomocna;
    unsigned char pos=0;

    pomocna=0;      //dig
    pomocna=(unsigned long)buffer[pos++]<<8;
    pomocna+=(unsigned long)buffer[pos++];
    pomocna+=(unsigned long)buffer[pos++]<<24;
    pomocna+=(unsigned long)buffer[pos++]<<16;

    totalParts->dig=pomocna;

    pomocna=0;      //dec
    pomocna=(unsigned long)buffer[pos++]<<8;
    pomocna+=(unsigned long)buffer[pos++];
    pomocna+=(unsigned long)buffer[pos++]<<24;
    pomocna+=(unsigned long)buffer[pos++]<<16;

    totalParts->dec=pomocna;
}

//------------------------------------------------------------------------------
//  Cteni casti prutoku MagX2.
//------------------------------------------------------------------------------
void MagX2Comm_ReadFlowParts(TMagX2CommDataHandler handler)
{
    unsigned char buff[8];

    buff[0]=SLAVE_ID;
    buff[1]=READ_HOLDING_REGISTER;

    buff[2]=(unsigned char)(MAGX2_MODBUS_MEAS_FLOW_ABS >> 8);
    buff[3]=(unsigned char)(MAGX2_MODBUS_MEAS_FLOW_ABS);

    buff[4]=0x00;
    buff[5]=4;

    unsigned int crcecko=usMBCRC16(buff,6);   //kontrolni soucet
    buff[6]=(unsigned char)(crcecko);
    buff[7]=(unsigned char)(crcecko>>8);

    SendData(buff, 8, handler);
}

//------------------------------------------------------------------------------
//  Parsing casti prutoku.
//------------------------------------------------------------------------------
void MagX2Comm_ParseFlowParts(const unsigned char *buffer, TFlowParts *flowParts)
{
    unsigned long pomocna;
    unsigned char pos=0;

    pomocna=0;      //abs
    pomocna=(unsigned long)buffer[pos++]<<8;
    pomocna+=(unsigned long)buffer[pos++];
    pomocna+=(unsigned long)buffer[pos++]<<24;
    pomocna+=(unsigned long)buffer[pos++]<<16;

    flowParts->abs=pomocna;

    pomocna=0;      //sign
    pomocna=(unsigned long)buffer[pos++]<<8;
    pomocna+=(unsigned long)buffer[pos++];
    pomocna+=(unsigned long)buffer[pos++]<<24;
    pomocna+=(unsigned long)buffer[pos++]<<16;

    flowParts->sign=pomocna;
}

//------------------------------------------------------------------------------
//  Cteni prutoku MagX2.
//------------------------------------------------------------------------------
void MagX2Comm_ReadFlowFloat(TMagX2CommDataHandler handler)
{
    ReadHoldingDWord(MAGX2_MODBUS_MEAS_FLOW_FLOAT, handler);
}

//------------------------------------------------------------------------------
//  Parsing prutoku.
//------------------------------------------------------------------------------
float MagX2Comm_ParseFlowFloat(const unsigned char *buffer)
{
    unsigned long pom = MagX2Comm_ParseUlong(buffer);
    return UlongToFloat(pom);
}

//------------------------------------------------------------------------------
//  Cteni Total MagX2.
//------------------------------------------------------------------------------
void MagX2Comm_ReadTotalFloat(TMagX2CommDataHandler handler)
{
    ReadHoldingDWord(MAGX2_MODBUS_MEAS_TOTAL_FLOAT, handler);
}

//------------------------------------------------------------------------------
//  Parsing Total.
//------------------------------------------------------------------------------
float MagX2Comm_ParseTotalFloat(const unsigned char *buffer)
{
    unsigned long pom = MagX2Comm_ParseUlong(buffer);
    return UlongToFloat(pom);
}

//------------------------------------------------------------------------------
//  Cteni Aux MagX2.
//------------------------------------------------------------------------------
void MagX2Comm_ReadAuxFloat(TMagX2CommDataHandler handler)
{
    ReadHoldingDWord(MAGX2_MODBUS_MEAS_AUX_FLOAT, handler);
}

//------------------------------------------------------------------------------
//  Parsing Aux.
//------------------------------------------------------------------------------
float MagX2Comm_ParseAuxFloat(const unsigned char *buffer)
{
    unsigned long pom = MagX2Comm_ParseUlong(buffer);
    return UlongToFloat(pom);
}

//------------------------------------------------------------------------------
//  Cteni Total+ MagX2.
//------------------------------------------------------------------------------
void MagX2Comm_ReadTotalPlusFloat(TMagX2CommDataHandler handler)
{
    ReadHoldingDWord(MAGX2_MODBUS_MEAS_POS_FLOAT, handler);
}

//------------------------------------------------------------------------------
//  Parsing Total+.
//------------------------------------------------------------------------------
float MagX2Comm_ParseTotalPlusFloat(const unsigned char *buffer)
{
    unsigned long pom = MagX2Comm_ParseUlong(buffer);
    return UlongToFloat(pom);
}

//------------------------------------------------------------------------------
//  Cteni Total- MagX2.
//------------------------------------------------------------------------------
void MagX2Comm_ReadTotalMinusFloat(TMagX2CommDataHandler handler)
{
    ReadHoldingDWord(MAGX2_MODBUS_MEAS_NEG_FLOAT, handler);
}

//------------------------------------------------------------------------------
//  Parsing Total-.
//------------------------------------------------------------------------------
float MagX2Comm_ParseTotalMinusFloat(const unsigned char *buffer)
{
    unsigned long pom = MagX2Comm_ParseUlong(buffer);
    return UlongToFloat(pom);
}

//------------------------------------------------------------------------------
//  Cteni teploty MagX2.
//------------------------------------------------------------------------------
void MagX2Comm_ReadTemperatureFloat(TMagX2CommDataHandler handler)
{
    ReadHoldingDWord(MAGX2_MODBUS_MEAS_TEMP_FLOAT, handler);
}

//------------------------------------------------------------------------------
//  Parsing teploty.
//------------------------------------------------------------------------------
float MagX2Comm_ParseTemperatureFloat(const unsigned char *buffer)
{
    unsigned long pom = MagX2Comm_ParseUlong(buffer);
    return UlongToFloat(pom);
}

//------------------------------------------------------------------------------
//  Cteni externi teploty MagX2.
//------------------------------------------------------------------------------
void MagX2Comm_ReadExtTemperatureFloat(TMagX2CommDataHandler handler)
{
    ReadHoldingDWord(MAGX2_MODBUS_EXT_MEAS_TEMP_FLOAT, handler);
}

//------------------------------------------------------------------------------
//  Parsing externi teploty.
//------------------------------------------------------------------------------
float MagX2Comm_ParseExtTemperatureFloat(const unsigned char *buffer)
{
    unsigned long pom = MagX2Comm_ParseUlong(buffer);
    return UlongToFloat(pom);
}

//------------------------------------------------------------------------------
//  Cteni externiho tlaku MagX2.
//------------------------------------------------------------------------------
void MagX2Comm_ReadExtPressureFloat(TMagX2CommDataHandler handler)
{
    ReadHoldingDWord(MAGX2_MODBUS_EXT_MEAS_PRESS_FLOAT, handler);
}

//------------------------------------------------------------------------------
//  Parsing externiho tlaku.
//------------------------------------------------------------------------------
float MagX2Comm_ParseExtPressureFloat(const unsigned char *buffer)
{
    unsigned long pom = MagX2Comm_ParseUlong(buffer);
    return UlongToFloat(pom);
}

