/******************************************************************************
*** usart.h ********************************************************
*******************************************************************************
*   Rozhrani pro main.c                                                       *
*                                                                             *
******************************************************************************/

#ifndef usart
#define usart


#define BUFFER_SIZE  (256)   // Velikost eru pro komunikaci se sensorem
typedef struct TBuffer
{
  unsigned char Buffer[BUFFER_SIZE];
  unsigned char Count;
}TBuffer;

extern volatile TBuffer GsmBuffer;
extern volatile TBuffer MBBuffer;
void usart_init_mcu(unsigned int baudrate);
void usart_init_gsm(unsigned int baudrate);
void mcu_send_c(unsigned char data);
void mcu_send_s(char * data);
void mcu_send_data(unsigned char *buff, unsigned int size);
void gsm_send_c(unsigned char data);
void gsm_send_s(char * data);
bool gsm_data_ready(void);

#endif //usart
