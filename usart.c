/******************************************************************************`
*** usart.c ********************************************************************
*******************************************************************************
*   Tento kod pracuje zobrazuje zpracova, a zobrazuje menu                    *
*                                                                             *
*   pouzity hardware:                                                         *
*     -modul procesoru MSP430F149                                             *
*     -displej DataImage TG13653                                              *
*                                                                             *
******************************************************************************/

#include <avr/io.h>
#include <stdbool.h>
#include "usart.h"

#define UART_BAUD_CALC(UART_BAUD_RATE) \
    ( ( F_CPU ) / ( ( UART_BAUD_RATE ) * 16UL ) - 1 )


void usart_init_mcu(unsigned int baudrate)
{
  UBRR0H = UART_BAUD_CALC(baudrate)>>8;       // set baudrate
  UBRR0L = UART_BAUD_CALC(baudrate);          // set baudrate
  UCSR0B = (1<<RXEN0) | (1<<TXEN0) | (1<<RXCIE0); // Enable receiver and transmitter and interrupt for RX complete
  UCSR0C = (3<<UCSZ00);            // set 8 bit, 1 stop bit, non parity
}

void usart_init_gsm(unsigned int baudrate)
{
  UBRR1H = UART_BAUD_CALC(baudrate)>>8;       // set baudrate
  UBRR1L = UART_BAUD_CALC(baudrate);          // set baudrate
  UCSR1B = (1<<RXEN1) | (1<<TXEN1) | (1<<RXCIE1); // Enable receiver and transmitter and interrupt for RX complete
  UCSR1C = (3<<UCSZ10);            // set 8 bit, 1 stop bit, non parity
}
void mcu_send_c(unsigned char data)
{
  while ( !( UCSR0A & (1<<UDRE0)) );
  UDR0 = data;
}

void mcu_send_s(char * data)
{
  while(*data)
  {
    mcu_send_c(*data);
    data++;
  }
}

void mcu_send_data(unsigned char *buff, unsigned int size)
{
  for (unsigned int i = 0; i < size; ++i)
    mcu_send_c(buff[i]);
}

void gsm_send_c(unsigned char data)
{
  while ( !( UCSR1A & (1<<UDRE1)) );
  UDR1 = data;
}

void gsm_send_s(char * data)
{
  while(*data)
  {
    gsm_send_c(*data);
    data++;
  }
}

bool gsm_data_ready()
{
  if (GsmBuffer.Count > 0)
    return true;
  else
    return false;
}
