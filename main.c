/********************************************************************************************
Program   : Arkon production : firmware pro rizeni prenosu modulu QUECTEL M95
Version   : v1.0
MCU       : AVR ATMEGA644
Author    : Lukas Vacha
Date      : 13.8.2012

TODO      : kdyz nastane chyba 322- memory full, tak poslat do LPC a zahlasit nejakym zpusobe,
            ted jenom timeoutne GSM modul na odeslani SMS...
*********************************************************************************************/

//#define DEBUG

#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdbool.h>
#include <ctype.h>
#include <util/delay.h>
#include <util/atomic.h>
#include <avr/pgmspace.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "usart.h"
#include "eeprom.h"
#include "sys_tick.h"
#include "magx2_comm.h"

#define VETSITKO_TIMEOUT    1
#define VETSITKO_OK         2
#define VETSITKO_ERROR      3

#define TIMEOUT_TIMER_PRESCALER      ( 256UL )
#define TIMEOUT_TIMER_TICKS          ( F_CPU / TIMEOUT_TIMER_PRESCALER )
#define TIMEOUT_VALUE                ( 1L )     //ms    //TIMEOUT POOLING
#define GPRS_CONNECTIONS_TIMEOUT  ( 10UL* 60000UL )   //1min

#define GPRS_TIMEOUT        50      //2-254
#define MB_TIMEOUT          5       //2-254 ms

#define LPC_STATUS_PIN_NUMBER   4
#define LPC_STATUS_PIN      PINB
#define LPC_STATUS_PORT     PORTB
#define LPC_STATUS_DDR      DDRB

#define QUECTEL_STATUS_PIN_NUMBER  0
#define QUECTEL_STATUS_PIN         PINC
#define QUECTEL_STATUS_PORT        PORTC
#define QUECTEL_STATUS_DDR         DDRC

#define GSM_POWER_PIN_NUMBER  1
#define GSM_POWER_PIN         PINC
#define GSM_POWER_PORT        PORTC
#define GSM_POWER_DDR         DDRC

#define GSM_STATUS_PIN_NUMBER 5
#define GSM_STATUS_PIN        PIND
#define GSM_STATUS_PORT       PORTD
#define GSM_STATUS_DDR        DDRD

#define ERR_GSM_SIGNAL         (1<<9)
#define ERR_GSM_SIMCARD        (1<<10)
#define ERR_GSM_SENDING        (1<<11)
#define ERR_GSM_OTHERS         (1<<12)

#define EE_SMS_PHONES_LGT   (3*16)
#define EE_STATION_ID_LGT   7
#define EE_APN_LGT          101
#define EE_IP_LGT           16
#define EE_PORT_LGT         6
#define EE_INTERVAL_LGT     2

#define SMS_START           1
#define SMS_STOP            0
#define SMS_STOP_ADDRESS    0
#define SMS_PHONE_ADDRESS   8
#define EE_STATION_ID_ADDR  (SMS_PHONE_ADDRESS + EE_SMS_PHONES_LGT)
#define EE_APN_ADDR         (EE_STATION_ID_ADDR + EE_STATION_ID_LGT)
#define EE_IP_ADDR          (EE_APN_ADDR + EE_APN_LGT)
#define EE_PORT_ADDR        (EE_IP_ADDR + EE_IP_LGT)
#define EE_INTERVAL_ADDR    (EE_PORT_ADDR + EE_PORT_LGT)

#define AT_RESPONSE_MSEC    300
#define AT_RESPONSE_REPEAT  10

#define IP_PORT_INVALID     0
#define INTERVAL_DEF        120

// max pocet timeoutu cteni hodnot
#define MAX_READ_VALUE_TIMEOUT_COUNT    3

typedef struct UnitInfo
{
  unsigned long unitNo, stationID;
  unsigned int year;
  unsigned int month, day, hour, min;
  float flowRate, total, aux, totalPos, totalNeg;
  float temperature, extTemperature, extPressure;
  TTotalParts totalParts;
  TFlowParts flowParts;
  unsigned long diameter, errorMin, actualError;
  char apn[EE_APN_LGT], ip[EE_IP_LGT], port[EE_PORT_LGT];
} TUnitInfo;


//! ***************************************************************************
//!                            GLOBALNI PROMENNE
//! ***************************************************************************

static TUnitInfo unitInfo;
static unsigned long intervalSysTime;
static unsigned char magx2TimeoutCount;
static bool magx2Comm;

volatile TBuffer MBBuffer;
volatile TBuffer GsmBuffer;

bool sendSms = false;
unsigned char buff[257];
unsigned char dataLoggerBuff[250];

// GSM

#define AT_USART_BUFFER_SIZE        255
#define PHONE_NUMBER_BUFFER_SIZE    20

// timeouty [ms]
#define STD_AT_RESPONSE_TIMEOUT  5000 	// standardni lhuta AT odpovedi
#define CPIN_RESPONSE_TIMEOUT    20000  // lhuta CPIN odpovedi
#define AT_FRAME_END_TIMEOUT     1000   // detekce konce prijimaneho ramce

// Typ - Lhuta pro prijem AT odpovedi
typedef unsigned long TATResponseTime;

// Typ - Lhuta cekani
typedef unsigned long TWaitTime;

// Typ - udalost
typedef enum
{
  EVENT_NONE,  						// zadna udalost
  EVENT_USART_RCV_FRAME,  // prijaty novy nebo aktualizovany ramec z USARTu
  EVENT_AT_RESP_TIMEOUT,  // timeout AT odpovedi
  EVENT_WAIT_TIMEOUT		  // timeout cekani
} TEvent;

// Typ - prototyp funkce, ktera je volana po prijmu odpovedi na AT prikaz,
// pripadne po vyprseni lhuty pro prijem odpovedi.
//
// Parametry:
//    rcvFrame 		- prijaty ramec z USARTu
//    respTimeout - true, pokud vyprsela lhuta prijmu odpovedi
typedef void (*TATResponseHandler) (const char *rcvFrame, bool respTimeout);

// zadna odpoved na AT prikaz se neceka
#define NO_RESPONSE_HANDLER   ((TATResponseHandler)0)

// Typ - prototyp funkce, ktera je volana po vyprseni lhuty cekani.
typedef void (*TWaitHandler) (void);

// zadne cekani
#define NO_WAIT_HANDLER   ((TWaitHandler)0)

// Typ - USART bufer
typedef struct
{
  unsigned char Buffer[AT_USART_BUFFER_SIZE]; // bufer
  unsigned int Count; // pocet platnych bytu v buferu
  bool ByteReceived; // prisel byte
  unsigned long FrameEndTimer; // casovac detekce konce prijimaneho ramce
} TATUsartBuffer;

// Typ - Stav prijmu AT odpovedi
typedef struct
{
  TATResponseHandler Handler; // ovladac odpovedi
  TATResponseTime Timeout; // timeout odpovedi
  TATResponseTime Timer; // casovac odpovedi
} TATResponseState;

// Typ - Stav cekani
typedef struct
{
  TWaitHandler Handler; // ovladac timeoutu
  TWaitTime Timeout; // timeout cekani
  TWaitTime Timer; // casovac cekani
} TWaitState;

// Typ - Data info
typedef struct
{
  int smsId; // cislo SMS
  int phoneId; // index tlf. cisla
  int interval; // nastaveny interval posilani zprav
  char phoneNumber[PHONE_NUMBER_BUFFER_SIZE]; // tlf. cislo odpovedi
  char tempPhoneNumber[PHONE_NUMBER_BUFFER_SIZE]; // pomocne tlf. cislo

} TGSMDataInfo;

// retezce
static const char OK_TXT[] = "\r\nOK\r\n";

// promenne
static volatile TATUsartBuffer atUsartBuffer;
static volatile TATResponseState atResponseState;
static volatile TWaitState atWaitState;
static volatile TGSMDataInfo gsmDataInfo;

//! **************************************************************************************
//!                                FUNKCE
//! **************************************************************************************

static bool MBGetData(unsigned char *buff, unsigned char *size, unsigned char maxSize);
static bool MBClearBuff(void);
static void OnSendCommand(void);
static void GsmSendC(unsigned char c);
static void GsmSendS(char *buff);
static unsigned int CopyUlongToBuf(char *buf, unsigned long value);
static unsigned int CopyFloatToBuf(char *buf, float value);
static unsigned int CopyLongToBuf(char *buf, long value);
static void CreateSmsInBuf(char *buf, const TUnitInfo *pUnitInfo);
static bool ReadParamsFromEEPROM(TUnitInfo *pUnitInfo);
static bool ReadPortFromEEPROM(TUnitInfo *pUnitInfo);
static bool ReadIpFromEEPROM(TUnitInfo *pUnitInfo);
static bool ReadApnFromEEPROM(TUnitInfo *pUnitInfo);
static bool ReadStationIdFromEEPROM(TUnitInfo *pUnitInfo);
static bool ProcessSMS_SetPhone(const char *msgTxt);
static void AT_CMGS_Begin_Response_SetPhone(const char *rcvFrame, bool respTimeout);
static bool ProcessSMS_SetGsmService(const char *msgTxt);
static void AT_CMGS_Begin_Response_SetGsmService(const char *rcvFrame, bool respTimeout);
static bool ProcessSMS_SetInterval(const char *msgTxt);
static void AT_CMGS_Begin_Response_SetInterval(const char *rcvFrame, bool respTimeout);
static bool ProcessSMS_StopSms(const char *msgTxt);
static void AT_CMGS_Begin_Response_StopSms(const char *rcvFrame, bool respTimeout);
static bool ProcessSMS_StartSms(const char *msgTxt);
static void AT_CMGS_Begin_Response_StartSms(const char *rcvFrame, bool respTimeout);
static bool ProcessSMS_SetPort(const char *msgTxt);
static void AT_CMGS_Begin_Response_SetPort(const char *rcvFrame, bool respTimeout);
static bool ProcessSMS_SetIp(const char *msgTxt);
static void AT_CMGS_Begin_Response_SetIp(const char *rcvFrame, bool respTimeout);
static bool ProcessSMS_SetApn(const char *msgTxt);
static void AT_CMGS_Begin_Response_SetApn(const char *rcvFrame, bool respTimeout);
static bool ProcessSMS_SetId(const char *msgTxt);
static void AT_CMGS_Begin_Response_SetId(const char *rcvFrame, bool respTimeout);
static void McuTask(void);
static void InitMagx2Comm(void);
static void WriteMagx2Passwords(void);
static void ReadMagx2Date(void);
static void ReadMagx2Time(void);
static void ReadMagx2FlowFloat(void);
static void ReadMagx2TotalFloat(void);
static void ReadMagx2AuxFloat(void);
static void ReadMagx2TotalPlusFloat(void);
static void ReadMagx2TotalMinusFloat(void);
static void ReadMagx2TemperatureFloat(void);
static void ReadMagx2ExtTemperatureFloat(void);
static void ReadMagx2ExtPressureFloat(void);
static void ReadMagx2UnitNo(void);
static void ReadMagx2InitUnitNo(void);
static void ReadMagx2TotalParts(void);
static void ReadMagx2FlowParts(void);
static void ReadMagx2Diameter(void);
static void ReadMagx2ErrorMin(void);
static void ReadMagx2ActualError(void);
void GSM_Init(void);
void GSM_Task(void);
void Process_USART_RCV_FRAME_Event(void);
void Process_AT_RESP_TIMEOUT_Event(void);
void Process_WAIT_TIMEOUT_Event(void);
void SetATResponse(TATResponseHandler handler, TATResponseTime timeout);
bool ContainsResponseOK(const char *str);
bool ContainsResponse(const char *str, const char *response);
void Wait(TWaitHandler handler, TWaitTime timeout);
void Wait_AfterReset(void);
void Wait_AfterReset_Timeout(void);
void Wait_AfterError(void);
void Wait_AfterError_Timeout(void);
void SendATCommand(const char *command, TATResponseHandler handler, TATResponseTime timeout);
void AT_Command(void);
void AT_Response(const char *rcvFrame, bool respTimeout);
void ATE_Command(void);
void ATE_Response(const char *rcvFrame, bool respTimeout);
void ATZ_Command(void);
void ATZ_Response(const char *rcvFrame, bool respTimeout);
void AT_IPR_Command(void);
void AT_IPR_Response(const char *rcvFrame, bool respTimeout);
void AT_QIURC_Command(void);
void AT_QIURC_Response(const char *rcvFrame, bool respTimeout);
void AT_CMGF_Command(void);
void AT_CMGF_Response(const char *rcvFrame, bool respTimeout);
void AT_CMGL_Command(void);
void AT_CMGL_Response(const char *rcvFrame, bool respTimeout);
void AT_CMGL_ParseResponse(const char *rcvFrame);
void AT_CMGR_Command(int smsId);
void AT_CMGR_Response(const char *rcvFrame, bool respTimeout);
void AT_CMGR_ParseResponse(const char *rcvFrame);
void AT_CSCA_Command(const char *sca);
void AT_CSCA_Response(const char *rcvFrame, bool respTimeout);
void AT_CMGD_Command(int smsId);
void AT_CMGD_Response(const char *rcvFrame, bool respTimeout);
static void AT_CMGS_Begin(const char *phoneNumber, TATResponseHandler handler);
static void AT_CMGS_End(TATResponseHandler handler);
bool ParsePhoneNumber(const char *phoneTxt);
bool ParseSmsMessage(const char *msgTxt);
static void SendSmsToPhone1Test(void);
static void SendSmsToPhone1_Response(const char *rcvFrame, bool respTimeout);
static void SendSmsToPhone1_Response_End(const char *rcvFrame, bool respTimeout);
static void SendSmsToPhone2Test(void);
static void SendSmsToPhone2_Response(const char *rcvFrame, bool respTimeout);
static void SendSmsToPhone2_Response_End(const char *rcvFrame, bool respTimeout);
static void SendSmsToPhone3Test(void);
static void SendSmsToPhone3_Response(const char *rcvFrame, bool respTimeout);
static void SendSmsToPhone3_Response_End(const char *rcvFrame, bool respTimeout);
static void SendGprsTest(void);
static void AT_QIREGAPP(const char *apn);
static void AT_QIREGAPP_Response(const char *rcvFrame, bool respTimeout);
static void AT_CGREG(void);
static void AT_CGREG_Response(const char *rcvFrame, bool respTimeout);
static void AT_QIDEACT(TATResponseHandler handler);
static void AT_QIDEACT_Response(const char *rcvFrame, bool respTimeout);
static void AT_QIOPEN(const char *ip, const char *port);
static void AT_QIOPEN_Response(const char *rcvFrame, bool respTimeout);
static void AT_QISEND(const TUnitInfo *pUnitInfo);
static void AT_QISEND_Response(const char *rcvFrame, bool respTimeout);
static void AT_QISEND_Data(void);
static void AT_QISEND_Data_Response(const char *rcvFrame, bool respTimeout);
static void AT_QIDEACT_End_Response(const char *rcvFrame, bool respTimeout);

static unsigned char IsMcuCommandMode(void)
{
    if (!(LPC_STATUS_PIN &(1<<LPC_STATUS_PIN_NUMBER)))     //CS=0 -> command mode
        return 1;
    else
        return 0;
}

static unsigned char IsMcuDataMode(void)
{
    if ((LPC_STATUS_PIN &(1<<LPC_STATUS_PIN_NUMBER)))     //CS=0 -> command mode
        return 1;
    else
        return 0;
}

static unsigned char IsSmsEnabled(void)
{
    if (EEPROM_read(SMS_STOP_ADDRESS) != SMS_START)
        return 0;

    unsigned int interval = EEPROM_read_uint(EE_INTERVAL_ADDR);

    if (0 == interval)
        return 0;
    else
        return 1;
}

static unsigned char IntervalDelayExpired(void)
{
    unsigned int interval = EEPROM_read_uint(EE_INTERVAL_ADDR);
    if (interval == 0xFFFF)
        interval = INTERVAL_DEF;

    unsigned long delay = (unsigned long)60 * (unsigned long)1000 * (unsigned long)interval;
    return SysTick_IsSysMSecondTimeout(intervalSysTime, delay);
}

static void InitUnitInfo(void)
{
    unitInfo.actualError = 0;
    unitInfo.errorMin = 0;
    unitInfo.diameter = 0;
    unitInfo.temperature = 0;
    unitInfo.extTemperature = 0;
    unitInfo.extPressure = 0;
    unitInfo.totalNeg = 0;
    unitInfo.totalPos = 0;
    unitInfo.aux = 0;
    unitInfo.total = 0;
    unitInfo.flowRate = 0;
    unitInfo.min = 0;
    unitInfo.hour = 0;
    unitInfo.day = 0;
    unitInfo.month = 0;
    unitInfo.year = 0;
    unitInfo.unitNo = 0;
    unitInfo.stationID = 0;
    unitInfo.totalParts.dec = 0;
    unitInfo.totalParts.dig = 0;
    unitInfo.flowParts.abs = 0;
    unitInfo.flowParts.sign = 0;
    unitInfo.apn[0] = 0;
    unitInfo.ip[0] = 0;
    unitInfo.port[0] = 0;
}

/*****************************************************************************************
  ***************                          MAIN                            ***************
  ****************************************************************************************/

int main(void)
{
    ///Inicializace
    // pin PD4 musi byt ve stavu vysoko impedance = vstup, po resetu je
    GSM_STATUS_DDR  |= (1<<GSM_STATUS_PIN_NUMBER);  // BUSY pin nastavit na vystup
    GSM_STATUS_PORT |= (1<<GSM_STATUS_PIN_NUMBER);  // BUSY pin nastavit na log 1 aby LPC vedel ze modul jede
    LPC_STATUS_DDR &= ~(1<<LPC_STATUS_PIN_NUMBER);
    QUECTEL_STATUS_DDR &= ~(1<<QUECTEL_STATUS_PIN_NUMBER);
    //! zapnuti napajeni pro QUECTEL
    // - nikdy se nevypina protoze status pin nam nefunguje hlasi 1 ikdyz jeste neni zapnuty...
    GSM_POWER_DDR |= (1<<GSM_POWER_PIN_NUMBER);

    LPC_STATUS_PORT |= 1 << LPC_STATUS_PIN_NUMBER;    //zapni pull-up

    //! nastartovani QUECTEL modulu
    //while( (QUECTEL_STATUS_PIN&(1<<QUECTEL_STATUS_PIN_NUMBER)) == 0 );

    //! ceka na 1 od LPC
    //while( (LPC_STATUS_PIN&(1<<LPC_STATUS_PIN_NUMBER)) == 0 );

    //! inicializace UARTU 0 - komunikace s MB7 MCU
    usart_init_mcu(19200);
    //! END inicializace UARTU 0

    //! inicializace UARTU 1 - komunikace s GSM modulem
    usart_init_gsm(19200);
    //! END inicializace UARTU 1

    InitUnitInfo();

    SysTick_Init();
    MagX2Comm_Init();

    intervalSysTime = SysTick_GetSysMSeconds();
    magx2Comm = false;

    sei();

    GSM_Init();

    //test
    /*EEPROM_write(SMS_STOP_ADDRESS, SMS_START);
    EEPROM_write_uint(EE_INTERVAL_ADDR, 1);
    EEPROM_write_str(EE_STATION_ID_ADDR, "200299", EE_STATION_ID_LGT);
    EEPROM_write_str(EE_APN_ADDR, "internet.t-mobile.cz", EE_APN_LGT);
    EEPROM_write_str(EE_IP_ADDR, "84.242.82.84", EE_IP_LGT);
    EEPROM_write_str(EE_PORT_ADDR, "8080", EE_PORT_LGT);*/

    for (;;)
    {
        McuTask();
        MagX2Comm_Task();
        GSM_Task();
    }
}

/*********************************************************************************************
                                        END OF MAIN
 *********************************************************************************************/

void GsmSendC(unsigned char c)             // send byte over serial link
{
  gsm_send_c(c);

}

static bool IsMcuCommandDataValid(const unsigned char *buff, unsigned int count)
{
    if (count == 0)
        return true;

    if (buff[0] != 0x55)
        return false;

    if (count == 1)
        return true;

    unsigned char size = buff[1];
    if (size <= 3)
        return false;

    if (count == 2)
        return true;

    switch (buff[2])
    {
        //pritomnost modulu
        case 0:
        if (size==1+3)
        {
            return true;
        }
        break;

        //nazev GPRS brany
        case 2:
        if ((size>3)&&(size<=64+3))
        {
            return true;
        }
        break;

        //jmeno uzivatele
        case 3:
        if ((size>3)&&(size<=12+3))
        {
            return true;
        }
        break;

        //heslo
        case 4:
        if ((size>3)&&(size<=12+3))
        {
            return true;
        }
        break;

        //port
        case 5:
        if (size==4+3)
        {
            return true;
        }
        break;

        //error
        case 6:
        if (size==1+3)
        {
            return true;
        }
        break;

        //ip address
        case 7:
        if (size==4+3)
        {
            return true;
        }
        break;

        //PIN
        case 8:
        if (size==4+3)
        {
            return true;
        }
        break;
    }

    return false;
}

static bool IsMcuCommandDataComplete(const unsigned char *buff, unsigned int count)
{
    if (count < 2)
        return false;

    unsigned char size = buff[1];
    if (size <= count)
        return true;
    else
        return false;
}

void OnSendCommand(void)
{
    if (!IsMcuCommandDataValid((unsigned char *)MBBuffer.Buffer, MBBuffer.Count))
    {
        MBClearBuff();
        return;
    }

    if (!IsMcuCommandDataComplete((unsigned char *)MBBuffer.Buffer, MBBuffer.Count))
        return;

    unsigned char size;
    if ((MBGetData(buff,&size,255)) && (buff[0]==0x55) && (size==buff[1]))  //kontrola hlavicky
    {
        switch (buff[2])
        {
            //pritomnost modulu
            case 0:
            if (size==1+3)
            {
                buff[3]=1;
                mcu_send_data(buff,size);
            }
            break;

            //nazev GPRS brany
            case 2:
            if ((size>3)&&(size<=64+3))
            {
                mcu_send_data(buff,size);
            }
            break;

            //jmeno uzivatele
            case 3:
            if ((size>3)&&(size<=12+3))
            {
                mcu_send_data(buff,size);
            }
            break;

            //heslo
            case 4:
            if ((size>3)&&(size<=12+3))
            {
                mcu_send_data(buff,size);
            }
            break;

            //port
            case 5:
            if (size==4+3)
            {
                mcu_send_data(buff,size);
            }
            break;

            //error
            case 6:
            if (size==1+3)
            {
                buff[3]=0;//GPRSStatus;
                mcu_send_data(buff,size);
            }
            break;

            //ip address
            case 7:
            if (size==4+3)
            {//dat same 0xFF, 0 zapricini, ze se bude MB porad ptat
                buff[3] = 0xFF;//GPRSIPAddress>>24;
                buff[4] = 0xFF;//GPRSIPAddress>>16;
                buff[5] = 0xFF;//GPRSIPAddress>>8;
                buff[6] = 0xFF;//GPRSIPAddress;
                mcu_send_data(buff,size);
            }
            break;

            //PIN
            case 8:
            if (size==4+3)
            {
                //GPRSPin=buff[6];
                //GPRSPin|=(unsigned int)(buff[5])<<8;
                //MBPinEnter = true;    //PIN byl zadan, mozno poslat do SIM
                mcu_send_data(buff,size);
            }
            break;
        }
    }
}

///--------------------------------------------- ISR data od MCU -----------------------------------------------
ISR(USART0_RX_vect)
{
  // MCU RX is complete interrupt
  unsigned char data = UDR0;
  if (MBBuffer.Count<(sizeof(MBBuffer.Buffer)/sizeof(MBBuffer.Buffer[0]))-1)
  {
    MBBuffer.Buffer[MBBuffer.Count] = data;
    MBBuffer.Count++;
    MBBuffer.Buffer[MBBuffer.Count] = 0;
  }
}

ISR(USART1_RX_vect)
{
  // GSM RX is complete interrupt
  unsigned char data = UDR1;
  if (atUsartBuffer.Count<(sizeof(atUsartBuffer.Buffer)/sizeof(atUsartBuffer.Buffer[0]))-1)
  {
    atUsartBuffer.Buffer[atUsartBuffer.Count] = data;
    atUsartBuffer.Count++;
    atUsartBuffer.Buffer[atUsartBuffer.Count] = 0;
    atUsartBuffer.ByteReceived = true;
  }
}

bool MBGetData(unsigned char *buff, unsigned char *size, unsigned char maxSize)
{
  if (MBBuffer.Count > 0)
  {
    if (maxSize>MBBuffer.Count)
      *size = MBBuffer.Count;
    else
      *size = maxSize;
    memcpy((void*)buff, (void*)MBBuffer.Buffer, *size);
    MBClearBuff();
    return true;
  }
  else
    return false;
}

bool MBClearBuff(void)
{
  MBBuffer.Buffer[0]=0;
  MBBuffer.Count = 0;
  return true;
}

/*  odesli retezec*/
void GsmSendS(char *buff)
{
  unsigned char Index = 0;
  while (( buff[Index] != 0))  //odesila se jen kdyz neni aktivni spojeni v ODM.
  {
    GsmSendC(buff[Index]);
    Index++;
  }
}

/********************************************************************************************
Cteni dat z EEPROM
*********************************************************************************************/

static bool ReadStationIdFromEEPROM(TUnitInfo *pUnitInfo)
{
    char temp[EE_STATION_ID_LGT];

    if (!EEPROM_read_str(EE_STATION_ID_ADDR, temp, EE_STATION_ID_LGT))
        return false;

    int scanResult = sscanf(temp, "%ld", &pUnitInfo->stationID);
    if (scanResult != 1)
        return false;

    return true;
}

static bool ReadApnFromEEPROM(TUnitInfo *pUnitInfo)
{
    if (!EEPROM_read_str(EE_APN_ADDR, pUnitInfo->apn, EE_APN_LGT))
        return false;

    if (pUnitInfo->apn[0] == 0)
        return false;

    return true;
}

static bool IsIPbyteValid(int ipByte)
{
    if ((ipByte >= 0) && (ipByte <= 255))
        return true;
    else
        return false;
}

static bool IsIPportValid(long ipPort)
{
    if ((ipPort > IP_PORT_INVALID) && (ipPort <= 0xFFFF))
        return true;
    else
        return false;
}

static bool ReadIpFromEEPROM(TUnitInfo *pUnitInfo)
{
    if (!EEPROM_read_str(EE_IP_ADDR, pUnitInfo->ip, EE_IP_LGT))
        return false;

    int a, b, c, d;
    int scanResult = sscanf(pUnitInfo->ip, "%d.%d.%d.%d", &a, &b, &c, &d);
    if (scanResult != 4)
        return false;

    if (IsIPbyteValid(a) && IsIPbyteValid(b) && IsIPbyteValid(c) && IsIPbyteValid(d))
        return true;
    else
        return false;
}

static bool ReadPortFromEEPROM(TUnitInfo *pUnitInfo)
{
    if (!EEPROM_read_str(EE_PORT_ADDR, pUnitInfo->port, EE_PORT_LGT))
        return false;

    long port;
    int scanResult = sscanf(pUnitInfo->port, "%ld", &port);
    if (scanResult != 1)
        return false;

    if (IsIPportValid(port))
        return true;
    else
        return false;
}

static void ReadPhoneStrFromEEPROM(unsigned int phone_address, char *str)
{
    for (int i = 0; i < 16; i++)
    {
        unsigned char ch = EEPROM_read(phone_address + i);
        if ((ch == ':') || (i == 15))
        {
            str[i] = 0;
            return;
        }
        else
        {
            str[i] = ch;
        }
    }
}

static bool ReadParamsFromEEPROM(TUnitInfo *pUnitInfo)
{
    if (ReadStationIdFromEEPROM(pUnitInfo) &&
        ReadApnFromEEPROM(pUnitInfo) &&
        ReadIpFromEEPROM(pUnitInfo) &&
        ReadPortFromEEPROM(pUnitInfo))
        return true;
    else
        return false;
}

/********************************************************************************************
Vytvareni retezcu zprav dataloggeru
*********************************************************************************************/

static unsigned int CopyStrToBuf(char *buf, const char *str)
{
    unsigned int count = 0;

    while (*str)
    {
        *buf = *str;

        buf++;
        str++;
        count++;
    }

    return count;
}

static unsigned int CopyUlongToBuf(char *buf, unsigned long value)
{
    int count;

    count = sprintf(buf, "%ld", value);

    if (count > 0)
        return (unsigned int)count;
    else
    {
        buf[0] = 0;
        return 0;
    }
}

static unsigned int CopyLongToBuf(char *buf, long value)
{
    int count;

    count = sprintf(buf, "%ld", value);

    if (count > 0)
        return (unsigned int)count;
    else
    {
        buf[0] = 0;
        return 0;
    }
}

static unsigned int CopyFloatToBuf(char *buf, float value)
{
    int count;

    count = sprintf(buf, "%.3f", value);

    if (count > 0)
        return (unsigned int)count;
    else
    {
        buf[0] = 0;
        return 0;
    }
}

static void CreateSmsInBuf(char *buf, const TUnitInfo *pUnitInfo)
{
    sprintf(buf,
          "UNITNO %08ld %04d.%02d.%02d %02d:%02d FLOWRATE %.3f M3/H TOTALPOS %.3f M3 TOTALNEG %.3f M3",
          pUnitInfo->unitNo,
          pUnitInfo->year,
          pUnitInfo->month,
          pUnitInfo->day,
          pUnitInfo->hour,
          pUnitInfo->min,
          pUnitInfo->flowRate,
          pUnitInfo->totalPos,
          pUnitInfo->totalNeg);
}

static unsigned int CopyDateTimeToBuf(char *buf, const TUnitInfo *pUnitInfo)
{
    sprintf(buf, "%.2d", pUnitInfo->year % 100);
    sprintf(&buf[2], "%.2d", pUnitInfo->month);
    sprintf(&buf[4], "%.2d", pUnitInfo->day);
    sprintf(&buf[6], "%.2d", pUnitInfo->hour);
    sprintf(&buf[8], "%.2d", pUnitInfo->min);

    return 10;
}

static unsigned int CopyParityCheckToBuf(char *buf, unsigned char parityCheck)
{
    sprintf(buf, "%.2X", parityCheck);
    return 2;
}

static unsigned int CopyUlongHexToBuf(char *buf, unsigned long value)
{
    sprintf(buf, "%.8X", value);
    return 8;
}

static unsigned int CreateDLmsgPart(char *msg, const char *prefixString, const char *valueString)
{
    unsigned int count, allCount;

    count = CopyStrToBuf(msg, prefixString);
    allCount = count;
    msg += count;

    *msg++ = ':';
    allCount++;

    count = CopyStrToBuf(msg, valueString);
    allCount += count;
    msg += count;

    *msg = ';';
    allCount++;

    return allCount;
}

static unsigned char ComputeDLmessageParityCheck(const char *msg, unsigned int count)
{
    unsigned char parityCheck = 0;

    for (unsigned int i = 0; i < count; i++)
    {
        unsigned int chWord = msg[i] << 1;
        for (unsigned char b = 0; b < 8; b++)
        {
            chWord = chWord >> 1;
            if (chWord & 1) parityCheck++;
        }
    }

    return parityCheck;
}

static unsigned int CreateDLmessageSTB(char *msg, const TUnitInfo *pUnitInfo)
{
    char tempBuf[50];
    unsigned int allCount, lgtIndex;

    CopyUlongToBuf(tempBuf, pUnitInfo->stationID);
    allCount = CreateDLmsgPart(msg, "#STB", tempBuf);

    lgtIndex = allCount + 2;

    CopyUlongToBuf(tempBuf, 999);
    allCount += CreateDLmsgPart(&msg[allCount], "L", tempBuf);

    CopyDateTimeToBuf(tempBuf, pUnitInfo);
    allCount += CreateDLmsgPart(&msg[allCount], "TM", tempBuf);

    CopyFloatToBuf(tempBuf, pUnitInfo->flowRate);
    allCount += CreateDLmsgPart(&msg[allCount], "A03", tempBuf);

    CopyFloatToBuf(tempBuf, pUnitInfo->total);
    allCount += CreateDLmsgPart(&msg[allCount], "A04", tempBuf);

    CopyFloatToBuf(tempBuf, pUnitInfo->aux);
    allCount += CreateDLmsgPart(&msg[allCount], "A05", tempBuf);

    CopyFloatToBuf(tempBuf, pUnitInfo->totalPos);
    allCount += CreateDLmsgPart(&msg[allCount], "A06", tempBuf);

    CopyFloatToBuf(tempBuf, pUnitInfo->totalNeg);
    allCount += CreateDLmsgPart(&msg[allCount], "A07", tempBuf);

    CopyFloatToBuf(tempBuf, pUnitInfo->temperature);
    allCount += CreateDLmsgPart(&msg[allCount], "A08", tempBuf);

    CopyFloatToBuf(tempBuf, pUnitInfo->extTemperature);
    allCount += CreateDLmsgPart(&msg[allCount], "A09", tempBuf);

    CopyFloatToBuf(tempBuf, pUnitInfo->extPressure);
    allCount += CreateDLmsgPart(&msg[allCount], "A10", tempBuf);

    CopyUlongToBuf(tempBuf, pUnitInfo->unitNo);
    allCount += CreateDLmsgPart(&msg[allCount], "P01", tempBuf);

    CopyUlongToBuf(tempBuf, pUnitInfo->totalParts.dig);
    allCount += CreateDLmsgPart(&msg[allCount], "P02", tempBuf);

    CopyUlongToBuf(tempBuf, pUnitInfo->flowParts.abs);
    allCount += CreateDLmsgPart(&msg[allCount], "P03", tempBuf);

    CopyUlongToBuf(tempBuf, pUnitInfo->totalParts.dec);
    allCount += CreateDLmsgPart(&msg[allCount], "P04", tempBuf);

    CopyUlongToBuf(tempBuf, pUnitInfo->diameter);
    allCount += CreateDLmsgPart(&msg[allCount], "P05", tempBuf);

    CopyUlongToBuf(tempBuf, pUnitInfo->errorMin);
    allCount += CreateDLmsgPart(&msg[allCount], "P06", tempBuf);

    CopyUlongToBuf(tempBuf, pUnitInfo->flowParts.sign);
    allCount += CreateDLmsgPart(&msg[allCount], "P07", tempBuf);

    CopyUlongHexToBuf(tempBuf, pUnitInfo->actualError);
    allCount += CreateDLmsgPart(&msg[allCount], "P08", tempBuf);

    sprintf(tempBuf, "%.3d", allCount - 1);
    memcpy(&msg[lgtIndex], tempBuf, 3);

    unsigned char parityCheck = ComputeDLmessageParityCheck(&msg[1], allCount - 1);
    allCount += CopyParityCheckToBuf(&msg[allCount], parityCheck);

    msg[allCount] = '#';
    allCount++;

    return allCount;
}

/********************************************************************************************
GPRS komunikace
*********************************************************************************************/

static void AT_QIREGAPP(const char *apn)
{
    GsmSendS("AT+QIREGAPP=");
    GsmSendC('"');
    GsmSendS((char *)apn);
    GsmSendC('"');
    GsmSendS("\r\n");

    SetATResponse(AT_QIREGAPP_Response, STD_AT_RESPONSE_TIMEOUT);
}

static void AT_QIREGAPP_Response(const char *rcvFrame, bool respTimeout)
{
	if (ContainsResponseOK(rcvFrame))
    {
        AT_CGREG();
		return;
    }

	if (respTimeout)
        AT_CGREG();
}

static void AT_CGREG(void)
{
    GsmSendS("AT+CGREG=1\r\n");
    SetATResponse(AT_CGREG_Response, STD_AT_RESPONSE_TIMEOUT);
}

static void AT_CGREG_Response(const char *rcvFrame, bool respTimeout)
{
	if (ContainsResponseOK(rcvFrame))
    {
        AT_QIDEACT(AT_QIDEACT_Response);
		return;
    }

	if (respTimeout)
        AT_QIDEACT(AT_QIDEACT_Response);
}

static void AT_QIDEACT(TATResponseHandler handler)
{
    GsmSendS("AT+QIDEACT\r\n");
    SetATResponse(handler, 10000);
}

static void AT_QIDEACT_Response(const char *rcvFrame, bool respTimeout)
{
	if (ContainsResponse(rcvFrame, "DEACT OK"))
    {
        AT_QIOPEN(unitInfo.ip, unitInfo.port);
		return;
    }

	if (respTimeout)
        AT_QIOPEN(unitInfo.ip, unitInfo.port);
}

static void AT_QIOPEN(const char *ip, const char *port)
{
    GsmSendS("AT+QIOPEN=");
    GsmSendC('"');
    GsmSendS("TCP");
    GsmSendC('"');
    GsmSendC(',');
    GsmSendC('"');
    GsmSendS((char *)ip);
    GsmSendC('"');
    GsmSendC(',');
    GsmSendS((char *)port);
    GsmSendS("\r\n");

    SetATResponse(AT_QIOPEN_Response, 20000);
}

static void AT_QIOPEN_Response(const char *rcvFrame, bool respTimeout)
{
	if (ContainsResponse(rcvFrame, "CONNECT OK"))
    {
        AT_QISEND(&unitInfo);
		return;
    }

	if (respTimeout)
        AT_QISEND(&unitInfo);
}

static void AT_QISEND(const TUnitInfo *pUnitInfo)
{
    char temp[20];

    unsigned int msgLgt = CreateDLmessageSTB((char *)dataLoggerBuff, pUnitInfo);
    CopyUlongToBuf(temp, msgLgt);

    GsmSendS("AT+QISEND=");
    GsmSendS((char *)temp);
    GsmSendS("\r\n");

    SetATResponse(AT_QISEND_Response, STD_AT_RESPONSE_TIMEOUT);
}

static void AT_QISEND_Response(const char *rcvFrame, bool respTimeout)
{
	if (ContainsResponse(rcvFrame, ">"))
    {
        AT_QISEND_Data();
		return;
    }

	if (respTimeout)
        AT_QISEND_Data();
}

static void AT_QISEND_Data(void)
{
    GsmSendS((char *)dataLoggerBuff);
    SetATResponse(AT_QISEND_Data_Response, STD_AT_RESPONSE_TIMEOUT);
}

static void AT_QISEND_Data_Response(const char *rcvFrame, bool respTimeout)
{
	if (ContainsResponse(rcvFrame, "SEND OK"))
    {
        AT_QIDEACT(AT_QIDEACT_End_Response);
		return;
    }

	if (respTimeout)
        AT_QIDEACT(AT_QIDEACT_End_Response);
}

static void AT_QIDEACT_End_Response(const char *rcvFrame, bool respTimeout)
{
	if (ContainsResponse(rcvFrame, "DEACT OK"))
    {
        AT_CMGL_Command();
		return;
    }

	if (respTimeout)
        AT_CMGL_Command();
}

/********************************************************************************************
LPC komunikace - datovy mod
*********************************************************************************************/

// Hlavni uloha LPC komunikace
static void McuTask(void)
{
    if (!magx2Comm)
    {
        if (unitInfo.unitNo == 0)
        {
            InitMagx2Comm();
            ReadMagx2InitUnitNo();
        }
    }

    if (IsSmsEnabled())
    {
        if (!magx2Comm)
        {
            if (IntervalDelayExpired() && IsMcuDataMode())
            {
                  intervalSysTime = SysTick_GetSysMSeconds();
                  InitMagx2Comm();
                  WriteMagx2Passwords();
            }
        }
    }

    if (MBBuffer.Count > 0)
    {
        if (IsMcuCommandMode())     //CS=0 -> command mode
            OnSendCommand();
        else
            MagX2Comm_ProcessRcvData((char *)MBBuffer.Buffer, MBBuffer.Count);
    }
}

static void InitMagx2Comm(void)
{
    magx2Comm = true;
    magx2TimeoutCount = 0;
    MBClearBuff();
}

static void ExitMagx2Comm(void)
{
    magx2Comm = false;
    MBClearBuff();
}

static void SuccessEndMagx2Comm(void)
{
    magx2Comm = false;
    sendSms = true;
    MBClearBuff();
}

//------------------------------------------------------------------------------
//  Vycteni serioveho cisla magx2.
//------------------------------------------------------------------------------
static void ReadMagx2InitUnitNo_DataHandler(const TCommData *commData)
{
    if (commData->commResult == CRES_OK)
    {
        unitInfo.unitNo = MagX2Comm_ParseUnitNo(commData->buffer);
        ExitMagx2Comm();
    }
    else
    {
        magx2TimeoutCount++;
        if (magx2TimeoutCount <= MAX_READ_VALUE_TIMEOUT_COUNT)
            ReadMagx2InitUnitNo();
        else
            ExitMagx2Comm();
    }
}

static void ReadMagx2InitUnitNo(void)
{
    if (IsMcuDataMode())
        MagX2Comm_ReadUnitNo(ReadMagx2InitUnitNo_DataHandler);
    else
        ExitMagx2Comm();
}

//------------------------------------------------------------------------------
//  Zapis hesel magx2.
//------------------------------------------------------------------------------
static void WriteMagx2Passwords_DataHandler(const TCommData *commData)
{
    if (commData->commResult == CRES_OK)
    {
        InitMagx2Comm();
        ReadMagx2Date();
    }
    else
    {
        magx2TimeoutCount++;
        if (magx2TimeoutCount <= MAX_READ_VALUE_TIMEOUT_COUNT)
            WriteMagx2Passwords();
        else
            ExitMagx2Comm();
    }
}

static void WriteMagx2Passwords(void)
{
    if (IsMcuDataMode())
        MagX2Comm_WritePasswords(WriteMagx2Passwords_DataHandler);
    else
        ExitMagx2Comm();
}

//------------------------------------------------------------------------------
//  Vycteni datumu magx2.
//------------------------------------------------------------------------------
static void ParseDate(const TDateTime *pDateTime, TUnitInfo *pUnitInfo)
{
    unsigned long value = pDateTime->date;

    pUnitInfo->day = 10 * ((value % 256) / 16) + (value % 16);
    value = value / 256;
    pUnitInfo->month = 10 * ((value % 256) / 16) + (value % 16);
    value = value / 256;
    pUnitInfo->year = 10 * ((value % 256) / 16) + (value % 16);
    value = value / 256;
    pUnitInfo->year = pUnitInfo->year + 100 * (10 * ((value % 256) / 16) + (value % 16));
}

static void ReadMagx2Date_DataHandler(const TCommData *commData)
{
    if (commData->commResult == CRES_OK)
    {
        TDateTime dateTime;
        MagX2Comm_ParseDate(commData->buffer, &dateTime);
        ParseDate(&dateTime, &unitInfo);

        InitMagx2Comm();
        ReadMagx2Time();
    }
    else
    {
        magx2TimeoutCount++;
        if (magx2TimeoutCount <= MAX_READ_VALUE_TIMEOUT_COUNT)
            ReadMagx2Date();
        else
            ExitMagx2Comm();
    }
}

static void ReadMagx2Date(void)
{
    if (IsMcuDataMode())
        MagX2Comm_ReadDate(ReadMagx2Date_DataHandler);
    else
        ExitMagx2Comm();
}

//------------------------------------------------------------------------------
//  Vycteni casu magx2.
//------------------------------------------------------------------------------
static void ParseTime(const TDateTime *pDateTime, TUnitInfo *pUnitInfo)
{
    unsigned long value = pDateTime->time;

    //unsigned int sec = 10 * ((value % 256) / 16) + (value % 16);
    value = value / 256;
    pUnitInfo->min = 10 * ((value % 256) / 16) + (value % 16);
    value = value / 256;
    pUnitInfo->hour = 10 * ((value % 256) / 16) + (value % 16);
}

static void ReadMagx2Time_DataHandler(const TCommData *commData)
{
    if (commData->commResult == CRES_OK)
    {
        TDateTime dateTime;
        MagX2Comm_ParseTime(commData->buffer, &dateTime);
        ParseTime(&dateTime, &unitInfo);

        InitMagx2Comm();
        ReadMagx2FlowFloat();
    }
    else
    {
        magx2TimeoutCount++;
        if (magx2TimeoutCount <= MAX_READ_VALUE_TIMEOUT_COUNT)
            ReadMagx2Time();
        else
            ExitMagx2Comm();
    }
}

static void ReadMagx2Time(void)
{
    if (IsMcuDataMode())
        MagX2Comm_ReadTime(ReadMagx2Time_DataHandler);
    else
        ExitMagx2Comm();
}

//------------------------------------------------------------------------------
//  Vycteni prutoku magx2.
//------------------------------------------------------------------------------
static void ReadMagx2FlowFloat_DataHandler(const TCommData *commData)
{
    if (commData->commResult == CRES_OK)
    {
        unitInfo.flowRate = MagX2Comm_ParseFlowFloat(commData->buffer);
        InitMagx2Comm();
        ReadMagx2TotalFloat();
    }
    else
    {
        magx2TimeoutCount++;
        if (magx2TimeoutCount <= MAX_READ_VALUE_TIMEOUT_COUNT)
            ReadMagx2FlowFloat();
        else
            ExitMagx2Comm();
    }
}

static void ReadMagx2FlowFloat(void)
{
    if (IsMcuDataMode())
        MagX2Comm_ReadFlowFloat(ReadMagx2FlowFloat_DataHandler);
    else
        ExitMagx2Comm();
}

//------------------------------------------------------------------------------
//  Vycteni Total magx2.
//------------------------------------------------------------------------------
static void ReadMagx2TotalFloat_DataHandler(const TCommData *commData)
{
    if (commData->commResult == CRES_OK)
    {
        unitInfo.total = MagX2Comm_ParseTotalFloat(commData->buffer);
        InitMagx2Comm();
        ReadMagx2AuxFloat();
    }
    else
    {
        magx2TimeoutCount++;
        if (magx2TimeoutCount <= MAX_READ_VALUE_TIMEOUT_COUNT)
            ReadMagx2TotalFloat();
        else
            ExitMagx2Comm();
    }
}

static void ReadMagx2TotalFloat(void)
{
    if (IsMcuDataMode())
        MagX2Comm_ReadTotalFloat(ReadMagx2TotalFloat_DataHandler);
    else
        ExitMagx2Comm();
}

//------------------------------------------------------------------------------
//  Vycteni Aux magx2.
//------------------------------------------------------------------------------
static void ReadMagx2AuxFloat_DataHandler(const TCommData *commData)
{
    if (commData->commResult == CRES_OK)
    {
        unitInfo.aux = MagX2Comm_ParseAuxFloat(commData->buffer);
        InitMagx2Comm();
        ReadMagx2TotalPlusFloat();
    }
    else
    {
        magx2TimeoutCount++;
        if (magx2TimeoutCount <= MAX_READ_VALUE_TIMEOUT_COUNT)
            ReadMagx2AuxFloat();
        else
            ExitMagx2Comm();
    }
}

static void ReadMagx2AuxFloat(void)
{
    if (IsMcuDataMode())
        MagX2Comm_ReadAuxFloat(ReadMagx2AuxFloat_DataHandler);
    else
        ExitMagx2Comm();
}

//------------------------------------------------------------------------------
//  Vycteni Total+ magx2.
//------------------------------------------------------------------------------
static void ReadMagx2TotalPlusFloat_DataHandler(const TCommData *commData)
{
    if (commData->commResult == CRES_OK)
    {
        unitInfo.totalPos = MagX2Comm_ParseTotalPlusFloat(commData->buffer);
        InitMagx2Comm();
        ReadMagx2TotalMinusFloat();
    }
    else
    {
        magx2TimeoutCount++;
        if (magx2TimeoutCount <= MAX_READ_VALUE_TIMEOUT_COUNT)
            ReadMagx2TotalPlusFloat();
        else
            ExitMagx2Comm();
    }
}

static void ReadMagx2TotalPlusFloat(void)
{
    if (IsMcuDataMode())
        MagX2Comm_ReadTotalPlusFloat(ReadMagx2TotalPlusFloat_DataHandler);
    else
        ExitMagx2Comm();
}

//------------------------------------------------------------------------------
//  Vycteni Total- magx2.
//------------------------------------------------------------------------------
static void ReadMagx2TotalMinusFloat_DataHandler(const TCommData *commData)
{
    if (commData->commResult == CRES_OK)
    {
        unitInfo.totalNeg = MagX2Comm_ParseTotalMinusFloat(commData->buffer);
        InitMagx2Comm();
        ReadMagx2TemperatureFloat();
    }
    else
    {
        magx2TimeoutCount++;
        if (magx2TimeoutCount <= MAX_READ_VALUE_TIMEOUT_COUNT)
            ReadMagx2TotalMinusFloat();
        else
            ExitMagx2Comm();
    }
}

static void ReadMagx2TotalMinusFloat(void)
{
    if (IsMcuDataMode())
        MagX2Comm_ReadTotalMinusFloat(ReadMagx2TotalMinusFloat_DataHandler);
    else
        ExitMagx2Comm();
}

//------------------------------------------------------------------------------
//  Vycteni teploty magx2.
//------------------------------------------------------------------------------
static void ReadMagx2TemperatureFloat_DataHandler(const TCommData *commData)
{
    if (commData->commResult == CRES_OK)
    {
        unitInfo.temperature = MagX2Comm_ParseTemperatureFloat(commData->buffer);
        InitMagx2Comm();
        ReadMagx2ExtTemperatureFloat();
    }
    else
    {
        magx2TimeoutCount++;
        if (magx2TimeoutCount <= MAX_READ_VALUE_TIMEOUT_COUNT)
            ReadMagx2TemperatureFloat();
        else
            ExitMagx2Comm();
    }
}

static void ReadMagx2TemperatureFloat(void)
{
    if (IsMcuDataMode())
        MagX2Comm_ReadTemperatureFloat(ReadMagx2TemperatureFloat_DataHandler);
    else
        ExitMagx2Comm();
}

//------------------------------------------------------------------------------
//  Vycteni externi teploty magx2.
//------------------------------------------------------------------------------
static void ReadMagx2ExtTemperatureFloat_DataHandler(const TCommData *commData)
{
    if (commData->commResult == CRES_OK)
    {
        unitInfo.extTemperature = MagX2Comm_ParseExtTemperatureFloat(commData->buffer);
        InitMagx2Comm();
        ReadMagx2ExtPressureFloat();
    }
    else
    {
        magx2TimeoutCount++;
        if (magx2TimeoutCount <= MAX_READ_VALUE_TIMEOUT_COUNT)
            ReadMagx2ExtTemperatureFloat();
        else
            ExitMagx2Comm();
    }
}

static void ReadMagx2ExtTemperatureFloat(void)
{
    if (IsMcuDataMode())
        MagX2Comm_ReadExtTemperatureFloat(ReadMagx2ExtTemperatureFloat_DataHandler);
    else
        ExitMagx2Comm();
}

//------------------------------------------------------------------------------
//  Vycteni externiho tlaku magx2.
//------------------------------------------------------------------------------
static void ReadMagx2ExtPressureFloat_DataHandler(const TCommData *commData)
{
    if (commData->commResult == CRES_OK)
    {
        unitInfo.extPressure = MagX2Comm_ParseExtPressureFloat(commData->buffer);
        InitMagx2Comm();
        ReadMagx2UnitNo();
    }
    else
    {
        magx2TimeoutCount++;
        if (magx2TimeoutCount <= MAX_READ_VALUE_TIMEOUT_COUNT)
            ReadMagx2ExtPressureFloat();
        else
            ExitMagx2Comm();
    }
}

static void ReadMagx2ExtPressureFloat(void)
{
    if (IsMcuDataMode())
        MagX2Comm_ReadExtPressureFloat(ReadMagx2ExtPressureFloat_DataHandler);
    else
        ExitMagx2Comm();
}

//------------------------------------------------------------------------------
//  Vycteni serioveho cisla magx2.
//------------------------------------------------------------------------------
static void ReadMagx2UnitNo_DataHandler(const TCommData *commData)
{
    if (commData->commResult == CRES_OK)
    {
        unitInfo.unitNo = MagX2Comm_ParseUnitNo(commData->buffer);
        InitMagx2Comm();
        ReadMagx2TotalParts();
    }
    else
    {
        magx2TimeoutCount++;
        if (magx2TimeoutCount <= MAX_READ_VALUE_TIMEOUT_COUNT)
            ReadMagx2UnitNo();
        else
            ExitMagx2Comm();
    }
}

static void ReadMagx2UnitNo(void)
{
    if (IsMcuDataMode())
        MagX2Comm_ReadUnitNo(ReadMagx2UnitNo_DataHandler);
    else
        ExitMagx2Comm();
}

//------------------------------------------------------------------------------
//  Vycteni casti Total magx2.
//------------------------------------------------------------------------------
static void ReadMagx2TotalParts_DataHandler(const TCommData *commData)
{
    if (commData->commResult == CRES_OK)
    {
        TTotalParts totalParts;
        MagX2Comm_ParseTotalParts(commData->buffer, &totalParts);
        unitInfo.totalParts.dig = totalParts.dig;
        unitInfo.totalParts.dec = totalParts.dec;

        InitMagx2Comm();
        ReadMagx2FlowParts();
    }
    else
    {
        magx2TimeoutCount++;
        if (magx2TimeoutCount <= MAX_READ_VALUE_TIMEOUT_COUNT)
            ReadMagx2TotalParts();
        else
            ExitMagx2Comm();
    }
}

static void ReadMagx2TotalParts(void)
{
    if (IsMcuDataMode())
        MagX2Comm_ReadTotalParts(ReadMagx2TotalParts_DataHandler);
    else
        ExitMagx2Comm();
}

//------------------------------------------------------------------------------
//  Vycteni casti prutoku magx2.
//------------------------------------------------------------------------------
static void ReadMagx2FlowParts_DataHandler(const TCommData *commData)
{
    if (commData->commResult == CRES_OK)
    {
        TFlowParts flowParts;
        MagX2Comm_ParseFlowParts(commData->buffer, &flowParts);
        unitInfo.flowParts.abs = flowParts.abs;
        unitInfo.flowParts.sign = flowParts.sign;

        InitMagx2Comm();
        ReadMagx2Diameter();
    }
    else
    {
        magx2TimeoutCount++;
        if (magx2TimeoutCount <= MAX_READ_VALUE_TIMEOUT_COUNT)
            ReadMagx2FlowParts();
        else
            ExitMagx2Comm();
    }
}

static void ReadMagx2FlowParts(void)
{
    if (IsMcuDataMode())
        MagX2Comm_ReadFlowParts(ReadMagx2FlowParts_DataHandler);
    else
        ExitMagx2Comm();
}

//------------------------------------------------------------------------------
//  Vycteni diameteru magx2.
//------------------------------------------------------------------------------
static void ReadMagx2Diameter_DataHandler(const TCommData *commData)
{
    if (commData->commResult == CRES_OK)
    {
        unitInfo.diameter = MagX2Comm_ParseDiameter(commData->buffer);
        InitMagx2Comm();
        ReadMagx2ErrorMin();
    }
    else
    {
        magx2TimeoutCount++;
        if (magx2TimeoutCount <= MAX_READ_VALUE_TIMEOUT_COUNT)
            ReadMagx2Diameter();
        else
            ExitMagx2Comm();
    }
}

static void ReadMagx2Diameter(void)
{
    if (IsMcuDataMode())
        MagX2Comm_ReadDiameter(ReadMagx2Diameter_DataHandler);
    else
        ExitMagx2Comm();
}

//------------------------------------------------------------------------------
//  Vycteni Error min magx2.
//------------------------------------------------------------------------------
static void ReadMagx2ErrorMin_DataHandler(const TCommData *commData)
{
    if (commData->commResult == CRES_OK)
    {
        unitInfo.errorMin = MagX2Comm_ParseErrorMin(commData->buffer);
        InitMagx2Comm();
        ReadMagx2ActualError();
    }
    else
    {
        magx2TimeoutCount++;
        if (magx2TimeoutCount <= MAX_READ_VALUE_TIMEOUT_COUNT)
            ReadMagx2ErrorMin();
        else
            ExitMagx2Comm();
    }
}

static void ReadMagx2ErrorMin(void)
{
    if (IsMcuDataMode())
        MagX2Comm_ReadErrorMin(ReadMagx2ErrorMin_DataHandler);
    else
        ExitMagx2Comm();
}

//------------------------------------------------------------------------------
//  Vycteni actual Error magx2.
//------------------------------------------------------------------------------
static void ReadMagx2ActualError_DataHandler(const TCommData *commData)
{
    if (commData->commResult == CRES_OK)
    {
        unitInfo.actualError = MagX2Comm_ParseActualError(commData->buffer);
        InitMagx2Comm();
        SuccessEndMagx2Comm();
    }
    else
    {
        magx2TimeoutCount++;
        if (magx2TimeoutCount <= MAX_READ_VALUE_TIMEOUT_COUNT)
            ReadMagx2ActualError();
        else
            ExitMagx2Comm();
    }
}

static void ReadMagx2ActualError(void)
{
    if (IsMcuDataMode())
        MagX2Comm_ReadActualError(ReadMagx2ActualError_DataHandler);
    else
        ExitMagx2Comm();
}

/********************************************************************************************
GSM komunikace
*********************************************************************************************/

// -----------------------------------------------------------------------------
// Inicializace USART buferu
// -----------------------------------------------------------------------------
void InitAtUsartBuffer(void)
{
  atUsartBuffer.Count = 0;
  atUsartBuffer.FrameEndTimer = 0;
  atUsartBuffer.ByteReceived = false;
  atUsartBuffer.Buffer[0] = 0;
}

// -----------------------------------------------------------------------------
// Inicializace GSM info
// -----------------------------------------------------------------------------
void InitGsmInfo(void)
{
    gsmDataInfo.smsId = 0;
    gsmDataInfo.phoneNumber[0] = 0;
}

// -----------------------------------------------------------------------------
// Vycisteni USART buferu
// -----------------------------------------------------------------------------
void CleanAtUsartBuffer(void)
{
    ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
    {
        InitAtUsartBuffer();
	}
}

// -----------------------------------------------------------------------------
// Inicializace stavu prijmu AT odpovedi
// -----------------------------------------------------------------------------
void InitATResponseState(void)
{
  atResponseState.Handler = NO_RESPONSE_HANDLER;
  atResponseState.Timeout = 0;
  atResponseState.Timer = 0;
}

// -----------------------------------------------------------------------------
// Inicializace stavu cekani
// -----------------------------------------------------------------------------
void InitATWaitState(void)
{
  atWaitState.Handler = NO_WAIT_HANDLER;
  atWaitState.Timeout = 0;
  atWaitState.Timer = 0;
}

// -----------------------------------------------------------------------------
// Nastaveni parametru AT odpovedi
// -----------------------------------------------------------------------------
void SetATResponse(TATResponseHandler handler, TATResponseTime timeout)
{
    ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
	{
	    InitAtUsartBuffer();
	    atUsartBuffer.FrameEndTimer = SysTick_GetSysMSeconds();

		atResponseState.Handler = handler;
        atResponseState.Timeout = timeout;
        atResponseState.Timer = SysTick_GetSysMSeconds();

		atWaitState.Handler = NO_WAIT_HANDLER;
	}
}

// -----------------------------------------------------------------------------
// Nastaveni parametru cekani
// -----------------------------------------------------------------------------
void Wait(TWaitHandler handler, TWaitTime timeout)
{
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
	{
		atWaitState.Handler = handler;
        atWaitState.Timeout = timeout;
        atWaitState.Timer = SysTick_GetSysMSeconds();

		atResponseState.Handler = NO_RESPONSE_HANDLER;
	}
}

// -----------------------------------------------------------------------------
// Odesle AT prikaz a nastavi parametry odpovedi
// -----------------------------------------------------------------------------
void SendATCommand(const char *command, TATResponseHandler handler, TATResponseTime timeout)
{
  GsmSendS((char *)command);
  SetATResponse(handler, timeout);
}

// -----------------------------------------------------------------------------
// Zjisti, zda retezec obsahuje OK odpoved na AT prikaz
// -----------------------------------------------------------------------------
bool ContainsResponseOK(const char *str)
{
    if (strstr(str, OK_TXT) != NULL)
        return true;
    else
		return false;
}

// -----------------------------------------------------------------------------
// Zjisti, zda retezec obsahuje text odpovedi na AT prikaz
// -----------------------------------------------------------------------------
bool ContainsResponse(const char *str, const char *response)
{
    if (strstr(str, response) != NULL)
        return true;
    else
		return false;
}

// -----------------------------------------------------------------------------
// Funkce cekani
// -----------------------------------------------------------------------------
// Cekani po resetu
// -----------------------------------------------------------------------------
void Wait_AfterReset(void)
{
  Wait(Wait_AfterReset_Timeout, 20000);
}

// -----------------------------------------------------------------------------
// Timeout cekani po resetu
// -----------------------------------------------------------------------------
void Wait_AfterReset_Timeout(void)
{
  AT_Command();
}

// -----------------------------------------------------------------------------
// Cekani po chybe
// -----------------------------------------------------------------------------
void Wait_AfterError(void)
{
  Wait(Wait_AfterError_Timeout, 5000);
}

// -----------------------------------------------------------------------------
// Timeout cekani po chybe
// -----------------------------------------------------------------------------
void Wait_AfterError_Timeout(void)
{
  AT_Command();
}

// -----------------------------------------------------------------------------
// AT prikazy a odpovedi
// -----------------------------------------------------------------------------
// AT prikaz
// -----------------------------------------------------------------------------
void AT_Command(void)
{
  SendATCommand("AT\r\n", AT_Response, STD_AT_RESPONSE_TIMEOUT);
}

// -----------------------------------------------------------------------------
// AT odpoved
// -----------------------------------------------------------------------------
void AT_Response(const char *rcvFrame, bool respTimeout)
{
	if (ContainsResponseOK(rcvFrame))
	{
        ATE_Command();
		return;
	}

	if (respTimeout)
        Wait_AfterError();
}

// -----------------------------------------------------------------------------
// ATE prikaz - Set Command Echo Mode
// -----------------------------------------------------------------------------
void ATE_Command(void)
{
  SendATCommand("ATE0\r\n", ATE_Response, STD_AT_RESPONSE_TIMEOUT);
}

// -----------------------------------------------------------------------------
// ATE odpoved
// -----------------------------------------------------------------------------
void ATE_Response(const char *rcvFrame, bool respTimeout)
{
	if (ContainsResponseOK(rcvFrame))
	{
        ATZ_Command();
		return;
	}

	if (respTimeout)
        AT_Command();
}

// -----------------------------------------------------------------------------
// ATZ prikaz - Set all Current Parameters to User Defined Profile
// -----------------------------------------------------------------------------
void ATZ_Command(void)
{
  SendATCommand("ATZ\r\n", ATZ_Response, STD_AT_RESPONSE_TIMEOUT);
}

// -----------------------------------------------------------------------------
// ATZ odpoved
// -----------------------------------------------------------------------------
void ATZ_Response(const char *rcvFrame, bool respTimeout)
{
	if (ContainsResponseOK(rcvFrame))
	{
        AT_IPR_Command();
		return;
	}

	if (respTimeout)
        AT_Command();
}

// -----------------------------------------------------------------------------
// AT+IPR prikaz - Set Fixed Local Rate
// -----------------------------------------------------------------------------
void AT_IPR_Command(void)
{
  SendATCommand("AT+IPR=19200;&W\r\n", AT_IPR_Response, STD_AT_RESPONSE_TIMEOUT);
}

// -----------------------------------------------------------------------------
// AT+IPR odpoved
// -----------------------------------------------------------------------------
void AT_IPR_Response(const char *rcvFrame, bool respTimeout)
{
	if (ContainsResponseOK(rcvFrame))
	{
        AT_QIURC_Command();
		return;
	}

	if (respTimeout)
        AT_Command();
}

// -----------------------------------------------------------------------------
// AT+QIURC prikaz - Disable Initial URC Presentation
// -----------------------------------------------------------------------------
void AT_QIURC_Command(void)
{
  SendATCommand("AT+QIURC=0;&W\r\n", AT_QIURC_Response, STD_AT_RESPONSE_TIMEOUT);
}

// -----------------------------------------------------------------------------
// AT+QIURC odpoved
// -----------------------------------------------------------------------------
void AT_QIURC_Response(const char *rcvFrame, bool respTimeout)
{
	if (ContainsResponseOK(rcvFrame))
	{
        AT_CMGF_Command();
		return;
	}

	if (respTimeout)
        AT_Command();
}

// -----------------------------------------------------------------------------
// AT+CMGF prikaz - Select SMS Message Format
// -----------------------------------------------------------------------------
void AT_CMGF_Command(void)
{
  SendATCommand("AT+CMGF=1\r\n", AT_CMGF_Response, STD_AT_RESPONSE_TIMEOUT);
}

// -----------------------------------------------------------------------------
// AT+CMGF odpoved
// -----------------------------------------------------------------------------
void AT_CMGF_Response(const char *rcvFrame, bool respTimeout)
{
	if (ContainsResponseOK(rcvFrame))
	{
        AT_CMGL_Command();
		return;
	}

	if (respTimeout)
        AT_Command();
}

// -----------------------------------------------------------------------------
// AT+CMGL prikaz - List SMS Messages from Preferred Store
// -----------------------------------------------------------------------------
void AT_CMGL_Command(void)
{
  SendATCommand("AT+CMGL=\"ALL\"\r\n", AT_CMGL_Response, STD_AT_RESPONSE_TIMEOUT);
}

// -----------------------------------------------------------------------------
// AT+CMGL odpoved
// -----------------------------------------------------------------------------
void AT_CMGL_Response(const char *rcvFrame, bool respTimeout)
{
	if (ContainsResponse(rcvFrame, "+CMGL:"))
	{
        AT_CMGL_ParseResponse(rcvFrame);
		return;
	}
	else if (ContainsResponseOK(rcvFrame))
    {
        if (sendSms)
        {
            sendSms = false;
            SendSmsToPhone1Test();
        }
        else
            AT_CMGL_Command();

		return;
    }

	if (respTimeout)
        AT_Command();
}

// -----------------------------------------------------------------------------
// Parsing odpovedi na AT+CMGL
// -----------------------------------------------------------------------------
void AT_CMGL_ParseResponse(const char *rcvFrame)
{
    char *p;
    p = strstr(rcvFrame, "+CMGL:");
    if (p == NULL)
    {
        AT_CMGL_Command();
    }
    else
    {
        int smsId = 0;
        sscanf(p, "+CMGL: %d,", &smsId);
        if (smsId == 0)
        {
            AT_CMGL_Command();
        }
        else
        {
            gsmDataInfo.smsId = smsId;
            AT_CMGR_Command(smsId);
        }
    }
}

// -----------------------------------------------------------------------------
// AT+CMGR prikaz - Read SMS Message
// -----------------------------------------------------------------------------
void AT_CMGR_Command(int smsId)
{
    char str[30];
    sprintf(str, "AT+CMGR=%d\r\n", smsId);
    SendATCommand(str, AT_CMGR_Response, STD_AT_RESPONSE_TIMEOUT);
}

// -----------------------------------------------------------------------------
// AT+CMGR odpoved
// -----------------------------------------------------------------------------
void AT_CMGR_Response(const char *rcvFrame, bool respTimeout)
{
	if (ContainsResponse(rcvFrame, "+CMGR:"))
	{
        AT_CMGR_ParseResponse(rcvFrame);
		return;
	}

	if (respTimeout)
        AT_Command();
}

// -----------------------------------------------------------------------------
// Parsing odpovedi na AT+CMGR
// -----------------------------------------------------------------------------
void AT_CMGR_ParseResponse(const char *rcvFrame)
{
    char *p;
    p = strstr(rcvFrame, "+CMGR:");
    if (p == NULL)
    {
        AT_CMGL_Command();
        return;
    }
    else
    {
        p = strstr(p, "\"");
        if (p == NULL)
        {
            AT_CMGL_Command();
            return;
        }
        else
        {
            p++;
            if (strncmp(p, "REC UNREAD", strlen("REC UNREAD")) == 0)
            {
                p += strlen("REC UNREAD") + 3;
                bool parse = ParsePhoneNumber(p);
                if (parse)
                {
                    p = strstr(p, "\r\n");
                    if (ParseSmsMessage(p))
                        return;
                }
            }
            else if (strncmp(p, "REC READ", strlen("REC READ")) == 0)
            {
                p += strlen("REC READ") + 3;
                bool parse = ParsePhoneNumber(p);
                if (parse)
                {
                    p = strstr(p, "\r\n");
                    if (ParseSmsMessage(p))
                        return;
                }
            }
        }
    }

    AT_CMGD_Command(gsmDataInfo.smsId);
}

// -----------------------------------------------------------------------------
// Parsing tlf. cisla (pokud neni NULL, konci znakem "), po uspechu vraci true
// -----------------------------------------------------------------------------
bool ParsePhoneNumber(const char *phoneTxt)
{
    if (phoneTxt == NULL)
        return false;

    char *p;
    p = strstr(phoneTxt, "\"");
    if (p == NULL)
        return false;

    size_t size = p - phoneTxt;
    if (size >= PHONE_NUMBER_BUFFER_SIZE)
        return false;

    memcpy((void *)gsmDataInfo.phoneNumber, phoneTxt, size);
    gsmDataInfo.phoneNumber[size] = 0;

    return true;
}

static void AT_CMGS_End_Response(const char *rcvFrame, bool respTimeout)
{
    if (ContainsResponseOK(rcvFrame))
	{
        AT_CMGD_Command(gsmDataInfo.smsId);
		return;
	}

	if (respTimeout)
        AT_CMGD_Command(gsmDataInfo.smsId);
}

// -----------------------------------------------------------------------------
// Parsing SMS zpravy (pokud neni NULL), po uspechu vraci true
// -----------------------------------------------------------------------------
bool ParseSmsMessage(const char *msgTxt)
{
    if (msgTxt == NULL)
        return false;

    char *p;

    if ((p = strstr(msgTxt, "SET PHONE")) != NULL)
    {
        return ProcessSMS_SetPhone(msgTxt);
    }
    else if ((p = strstr(msgTxt, "SET GSMSERVICE")) != NULL)
    {
        return ProcessSMS_SetGsmService(msgTxt);
    }
    else if ((p = strstr(msgTxt, "SET INTERVAL")) != NULL)
    {
        return ProcessSMS_SetInterval(msgTxt);
    }
    else if ((p = strstr(msgTxt, "STOP SMS")) != NULL)
    {
        return ProcessSMS_StopSms(msgTxt);
    }
    else if ((p = strstr(msgTxt, "START SMS")) != NULL)
    {
        return ProcessSMS_StartSms(msgTxt);
    }
    else if ((p = strstr(msgTxt, "SET PORT")) != NULL)
    {
        return ProcessSMS_SetPort(msgTxt);
    }
    else if ((p = strstr(msgTxt, "SET IP")) != NULL)
    {
        return ProcessSMS_SetIp(msgTxt);
    }
    else if ((p = strstr(msgTxt, "SET APN")) != NULL)
    {
        return ProcessSMS_SetApn(msgTxt);
    }
    else if ((p = strstr(msgTxt, "SET ID")) != NULL)
    {
        return ProcessSMS_SetId(msgTxt);
    }

    return false;
}

static bool ProcessSMS_SetPhone(const char *msgTxt)
{
    char *pMsg;
    long unitNo;

    if ((pMsg = strstr(msgTxt, "SET PHONE")) == NULL)
        return false;

    int scanResult = sscanf(pMsg, "SET PHONE%d %ld", &gsmDataInfo.phoneId, &unitNo);
    if (scanResult != 2)
        return false;

    if (unitNo != unitInfo.unitNo)
        return false;

    if ((gsmDataInfo.phoneId < 1) || (gsmDataInfo.phoneId > 3))
        return false;

    unsigned int phone_address = SMS_PHONE_ADDRESS;
    if (gsmDataInfo.phoneId == 1)
        phone_address = SMS_PHONE_ADDRESS;
    if (gsmDataInfo.phoneId == 2)
        phone_address = SMS_PHONE_ADDRESS+16;
    if (gsmDataInfo.phoneId == 3)
        phone_address = SMS_PHONE_ADDRESS+32;

    char *p1, *p2;
    if ((p1 = strstr(pMsg, "+")) != NULL)
    {
        p2 = strstr(p1, "\r\n");
        if (p2 == NULL)
            return false;

        size_t size = p2 - p1;
        if (size > 16)
            return false;

        memcpy((void *)gsmDataInfo.tempPhoneNumber, p1, size);
        gsmDataInfo.tempPhoneNumber[size] = 0;

        for (int i = 0; i < size; i++)
        {
            EEPROM_write(phone_address + i, p1[i]);
        }

        if (size < 16)
            EEPROM_write(phone_address + size, ':');
    }
    else if ((p1 = strstr(pMsg, "NONE")) != NULL)
    {
        size_t size = strlen("NONE");
        if (size >= PHONE_NUMBER_BUFFER_SIZE)
            return false;

        memcpy((void *)gsmDataInfo.tempPhoneNumber, p1, size);
        gsmDataInfo.tempPhoneNumber[size] = 0;

        EEPROM_write(phone_address, ':');
    }
    else
        return false;

    AT_CMGS_Begin((const char *)gsmDataInfo.phoneNumber, AT_CMGS_Begin_Response_SetPhone);
    return true;
}

static void AT_CMGS_Begin_Response_SetPhone(const char *rcvFrame, bool respTimeout)
{
    if (ContainsResponse(rcvFrame, ">") || respTimeout)
	{
        char temp[60];

        sprintf(temp, "PHONE%d %ld %s", gsmDataInfo.phoneId, unitInfo.unitNo, gsmDataInfo.tempPhoneNumber);
        GsmSendS(temp);

        AT_CMGS_End(AT_CMGS_End_Response);
	}
}

static bool ProcessSMS_SetGsmService(const char *msgTxt)
{
    char *pMsg;
    long unitNo;

    if ((pMsg = strstr(msgTxt, "SET GSMSERVICE")) == NULL)
        return false;

    int scanResult = sscanf(pMsg, "SET GSMSERVICE %ld", &unitNo);
    if (scanResult != 1)
        return false;

    if (unitNo != unitInfo.unitNo)
        return false;

    char *p1, *p2;
    p1 = strstr(pMsg, "+");
    if (p1 == NULL)
        return false;

    p2 = strstr(p1, "\r\n");
    if (p2 == NULL)
        return false;

    size_t size = p2 - p1;
    if (size >= PHONE_NUMBER_BUFFER_SIZE)
        return false;

    memcpy((void *)gsmDataInfo.tempPhoneNumber, p1, size);
    gsmDataInfo.tempPhoneNumber[size] = 0;

    AT_CSCA_Command((const char *)gsmDataInfo.tempPhoneNumber);
    return true;
}

static void AT_CMGS_Begin_Response_SetGsmService(const char *rcvFrame, bool respTimeout)
{
    if (ContainsResponse(rcvFrame, ">") || respTimeout)
	{
	    char temp[60];

        sprintf(temp, "GSMSERVICE %ld %s", unitInfo.unitNo, gsmDataInfo.tempPhoneNumber);
        GsmSendS(temp);

        AT_CMGS_End(AT_CMGS_End_Response);
	}
}

static bool ProcessSMS_SetInterval(const char *msgTxt)
{
    char *pMsg;
    long unitNo, interval;

    if ((pMsg = strstr(msgTxt, "SET INTERVAL")) == NULL)
        return false;

    int scanResult = sscanf(pMsg, "SET INTERVAL %ld %ld", &unitNo, &interval);
    if (scanResult != 2)
        return false;

    if (unitNo != unitInfo.unitNo)
        return false;

    gsmDataInfo.interval = (int)interval;
    if (gsmDataInfo.interval > 9999)
        return false;

    EEPROM_write_uint(EE_INTERVAL_ADDR, (unsigned int)interval);
    intervalSysTime = SysTick_GetSysMSeconds();

    if (EEPROM_read_uint(EE_INTERVAL_ADDR) != gsmDataInfo.interval)
        return false;

    AT_CMGS_Begin((const char *)gsmDataInfo.phoneNumber, AT_CMGS_Begin_Response_SetInterval);
    return true;
}

static void AT_CMGS_Begin_Response_SetInterval(const char *rcvFrame, bool respTimeout)
{
    if (ContainsResponse(rcvFrame, ">") || respTimeout)
	{
	    char temp[60];

        sprintf(temp, "INTERVAL %ld %d", unitInfo.unitNo, gsmDataInfo.interval);
        GsmSendS(temp);

        AT_CMGS_End(AT_CMGS_End_Response);
	}
}

static bool ProcessSMS_StopSms(const char *msgTxt)
{
    char *pMsg;
    long unitNo;

    if ((pMsg = strstr(msgTxt, "STOP SMS")) == NULL)
        return false;

    int scanResult = sscanf(pMsg, "STOP SMS %ld", &unitNo);
    if (scanResult != 1)
        return false;

    if (unitNo != unitInfo.unitNo)
        return false;

    EEPROM_write(SMS_STOP_ADDRESS, SMS_STOP);

    if (EEPROM_read(SMS_STOP_ADDRESS) != SMS_STOP)
        return false;

    AT_CMGS_Begin((const char *)gsmDataInfo.phoneNumber, AT_CMGS_Begin_Response_StopSms);
    return true;
}

static void AT_CMGS_Begin_Response_StopSms(const char *rcvFrame, bool respTimeout)
{
    if (ContainsResponse(rcvFrame, ">") || respTimeout)
	{
	    char temp[60];

        CopyUlongToBuf(temp, unitInfo.unitNo);
        GsmSendS(temp);

        GsmSendS(" SMS SENDING STOPPED");

        AT_CMGS_End(AT_CMGS_End_Response);
	}
}

static bool ProcessSMS_StartSms(const char *msgTxt)
{
    char *pMsg;
    long unitNo;

    if ((pMsg = strstr(msgTxt, "START SMS")) == NULL)
        return false;

    int scanResult = sscanf(pMsg, "START SMS %ld", &unitNo);
    if (scanResult != 1)
        return false;

    if (unitNo != unitInfo.unitNo)
        return false;

    EEPROM_write(SMS_STOP_ADDRESS, SMS_START);
    intervalSysTime = SysTick_GetSysMSeconds();

    if (EEPROM_read(SMS_STOP_ADDRESS) != SMS_START)
        return false;

    AT_CMGS_Begin((const char *)gsmDataInfo.phoneNumber, AT_CMGS_Begin_Response_StartSms);
    return true;
}

static void AT_CMGS_Begin_Response_StartSms(const char *rcvFrame, bool respTimeout)
{
    if (ContainsResponse(rcvFrame, ">") || respTimeout)
	{
	    char temp[60];

        CopyUlongToBuf(temp, unitInfo.unitNo);
        GsmSendS(temp);

        GsmSendS(" SMS SENDING STARTED");

        AT_CMGS_End(AT_CMGS_End_Response);
	}
}

static bool ProcessSMS_SetPort(const char *msgTxt)
{
    char temp[20];
    char *pMsg;
    long unitNo, port;

    if ((pMsg = strstr(msgTxt, "SET PORT")) == NULL)
        return false;

    int scanResult = sscanf(pMsg, "SET PORT %ld %ld", &unitNo, &port);
    if (scanResult != 2)
        return false;

    if (unitNo != unitInfo.unitNo)
        return false;

    if (port > 0xFFFF)
        return false;

    CopyLongToBuf(temp, port);
    EEPROM_write_str(EE_PORT_ADDR, temp, EE_PORT_LGT);

    if ((port != IP_PORT_INVALID) && (!ReadPortFromEEPROM(&unitInfo)))
        return false;

    AT_CMGS_Begin((const char *)gsmDataInfo.phoneNumber, AT_CMGS_Begin_Response_SetPort);
    return true;
}

static void AT_CMGS_Begin_Response_SetPort(const char *rcvFrame, bool respTimeout)
{
    if (ContainsResponse(rcvFrame, ">") || respTimeout)
	{
	    char temp[60];

        CopyUlongToBuf(temp, unitInfo.unitNo);
        GsmSendS(temp);

        GsmSendS(" PORT SET TO ");

        GsmSendS(unitInfo.port);

        AT_CMGS_End(AT_CMGS_End_Response);
	}
}

static bool ProcessSMS_SetIp(const char *msgTxt)
{
    char temp[25];
    char *pMsg;
    long unitNo;
    int a, b, c, d;

    if ((pMsg = strstr(msgTxt, "SET IP")) == NULL)
        return false;

    int scanResult = sscanf(pMsg, "SET IP %ld %d.%d.%d.%d", &unitNo, &a, &b, &c, &d);
    if (scanResult != 5)
        return false;

    if (unitNo != unitInfo.unitNo)
        return false;

    sprintf(temp, "%d.%d.%d.%d", a, b, c, d);
    EEPROM_write_str(EE_IP_ADDR, temp, EE_IP_LGT);

    if (!ReadIpFromEEPROM(&unitInfo))
        return false;

    AT_CMGS_Begin((const char *)gsmDataInfo.phoneNumber, AT_CMGS_Begin_Response_SetIp);
    return true;
}

static void AT_CMGS_Begin_Response_SetIp(const char *rcvFrame, bool respTimeout)
{
    if (ContainsResponse(rcvFrame, ">") || respTimeout)
	{
	    char temp[60];

        CopyUlongToBuf(temp, unitInfo.unitNo);
        GsmSendS(temp);

        GsmSendS(" IP SET TO ");

        GsmSendS(unitInfo.ip);

        AT_CMGS_End(AT_CMGS_End_Response);
	}
}

static bool ProcessSMS_SetApn(const char *msgTxt)
{
    char *pMsg;
    long unitNo;

    if ((pMsg = strstr(msgTxt, "SET APN")) == NULL)
        return false;

    int scanResult = sscanf(pMsg, "SET APN %ld %s", &unitNo, dataLoggerBuff);
    if (scanResult != 2)
        return false;

    if (unitNo != unitInfo.unitNo)
        return false;

    EEPROM_write_str(EE_APN_ADDR, (char *)dataLoggerBuff, EE_APN_LGT);

    if (!ReadApnFromEEPROM(&unitInfo))
        return false;

    AT_CMGS_Begin((const char *)gsmDataInfo.phoneNumber, AT_CMGS_Begin_Response_SetApn);
    return true;
}

static void AT_CMGS_Begin_Response_SetApn(const char *rcvFrame, bool respTimeout)
{
    if (ContainsResponse(rcvFrame, ">") || respTimeout)
	{
	    char temp[60];

        CopyUlongToBuf(temp, unitInfo.unitNo);
        GsmSendS(temp);

        GsmSendS(" APN SET TO ");

        GsmSendS(unitInfo.apn);

        AT_CMGS_End(AT_CMGS_End_Response);
	}
}

static bool ProcessSMS_SetId(const char *msgTxt)
{
    char temp[20];
    char *pMsg;
    long unitNo, stationId;

    if ((pMsg = strstr(msgTxt, "SET ID")) == NULL)
        return false;

    int scanResult = sscanf(pMsg, "SET ID %ld %ld", &unitNo, &stationId);
    if (scanResult != 2)
        return false;

    if (unitNo != unitInfo.unitNo)
        return false;

    CopyLongToBuf(temp, stationId);
    EEPROM_write_str(EE_STATION_ID_ADDR, temp, EE_STATION_ID_LGT);

    if (!ReadStationIdFromEEPROM(&unitInfo))
        return false;

    AT_CMGS_Begin((const char *)gsmDataInfo.phoneNumber, AT_CMGS_Begin_Response_SetId);
    return true;
}

static void AT_CMGS_Begin_Response_SetId(const char *rcvFrame, bool respTimeout)
{
    if (ContainsResponse(rcvFrame, ">") || respTimeout)
	{
	    char temp[60];

        CopyUlongToBuf(temp, unitInfo.unitNo);
        GsmSendS(temp);

        GsmSendS(" ID SET TO ");

        CopyUlongToBuf(temp, unitInfo.stationID);
        GsmSendS(temp);

        AT_CMGS_End(AT_CMGS_End_Response);
	}
}

// -----------------------------------------------------------------------------
// AT+CSCA prikaz - SMS Service Center Address
// -----------------------------------------------------------------------------
void AT_CSCA_Command(const char *sca)
{
    char str[50];
    sprintf(str, "AT+CSCA=\"%s\"\r\n", sca);
    SendATCommand(str, AT_CSCA_Response, STD_AT_RESPONSE_TIMEOUT);
}

// -----------------------------------------------------------------------------
// AT+CSCA odpoved
// -----------------------------------------------------------------------------
void AT_CSCA_Response(const char *rcvFrame, bool respTimeout)
{
	if (ContainsResponseOK(rcvFrame))
	{
        AT_CMGS_Begin((const char *)gsmDataInfo.phoneNumber, AT_CMGS_Begin_Response_SetGsmService);
		return;
	}

	if (respTimeout)
        AT_CMGD_Command(gsmDataInfo.smsId);
}

// -----------------------------------------------------------------------------
// AT+CMGD prikaz - Delete SMS Message
// -----------------------------------------------------------------------------
void AT_CMGD_Command(int smsId)
{
    char str[30];
    sprintf(str, "AT+CMGD=%d\r\n", smsId);
    SendATCommand(str, AT_CMGD_Response, STD_AT_RESPONSE_TIMEOUT);
}

// -----------------------------------------------------------------------------
// AT+CMGD odpoved
// -----------------------------------------------------------------------------
void AT_CMGD_Response(const char *rcvFrame, bool respTimeout)
{
	if (ContainsResponseOK(rcvFrame))
	{
        AT_CMGL_Command();
		return;
	}

	if (respTimeout)
        AT_Command();
}

// -----------------------------------------------------------------------------
// Odesilani SMS
// -----------------------------------------------------------------------------

static void AT_CMGS_Begin(const char *phoneNumber, TATResponseHandler handler)
{
    GsmSendS("AT+CMGS=\"");
    GsmSendS((char*)phoneNumber);
    GsmSendS("\"\r\n");

    SetATResponse(handler, STD_AT_RESPONSE_TIMEOUT);
}

static void AT_CMGS_End(TATResponseHandler handler)
{
    GsmSendC(0x1A);
    SetATResponse(handler, 20000);
}

static void SendSmsToPhone1Test(void)
{
    if (EEPROM_read(SMS_PHONE_ADDRESS) < ':' )
    {
        char temp[20];
        ReadPhoneStrFromEEPROM(SMS_PHONE_ADDRESS, temp);
        AT_CMGS_Begin(temp, SendSmsToPhone1_Response);
    }
    else
        SendSmsToPhone2Test();
}

static void SendSmsToPhone1_Response(const char *rcvFrame, bool respTimeout)
{
    if (ContainsResponse(rcvFrame, ">") || respTimeout)
	{
	    CreateSmsInBuf((char *)buff, &unitInfo);
        GsmSendS((char *)buff);

        AT_CMGS_End(SendSmsToPhone1_Response_End);
	}
}

static void SendSmsToPhone1_Response_End(const char *rcvFrame, bool respTimeout)
{
    if (ContainsResponseOK(rcvFrame) || respTimeout)
	{
	    SendSmsToPhone2Test();
	}
}

static void SendSmsToPhone2Test(void)
{
    if (EEPROM_read(SMS_PHONE_ADDRESS+16) < ':')
    {
        char temp[20];
        ReadPhoneStrFromEEPROM(SMS_PHONE_ADDRESS+16, temp);
        AT_CMGS_Begin(temp, SendSmsToPhone2_Response);
    }
    else
        SendSmsToPhone3Test();
}

static void SendSmsToPhone2_Response(const char *rcvFrame, bool respTimeout)
{
    if (ContainsResponse(rcvFrame, ">") || respTimeout)
	{
	    CreateSmsInBuf((char *)buff, &unitInfo);
        GsmSendS((char *)buff);

        AT_CMGS_End(SendSmsToPhone2_Response_End);
	}
}

static void SendSmsToPhone2_Response_End(const char *rcvFrame, bool respTimeout)
{
    if (ContainsResponseOK(rcvFrame) || respTimeout)
	{
	    SendSmsToPhone3Test();
	}
}

static void SendSmsToPhone3Test(void)
{
    if (EEPROM_read(SMS_PHONE_ADDRESS+32) < ':')
    {
        char temp[20];
        ReadPhoneStrFromEEPROM(SMS_PHONE_ADDRESS+32, temp);
        AT_CMGS_Begin(temp, SendSmsToPhone3_Response);
    }
    else
        SendGprsTest();
}

static void SendSmsToPhone3_Response(const char *rcvFrame, bool respTimeout)
{
    if (ContainsResponse(rcvFrame, ">") || respTimeout)
	{
	    CreateSmsInBuf((char *)buff, &unitInfo);
        GsmSendS((char *)buff);

        AT_CMGS_End(SendSmsToPhone3_Response_End);
	}
}

static void SendSmsToPhone3_Response_End(const char *rcvFrame, bool respTimeout)
{
    if (ContainsResponseOK(rcvFrame) || respTimeout)
	{
	    SendGprsTest();
	}
}

static void SendGprsTest(void)
{
    if (ReadParamsFromEEPROM(&unitInfo))
        AT_QIREGAPP(unitInfo.apn);
    else
        AT_CMGL_Command();
}

// -----------------------------------------------------------------------------
// Zjisti, zda nastala udalost EVENT_USART_RCV_FRAME
// -----------------------------------------------------------------------------
bool Is_USART_RCV_FRAME_Event(void)
{
    if ((atResponseState.Handler != NO_RESPONSE_HANDLER) &&
        (atUsartBuffer.Count > 0) &&
        (SysTick_IsSysMSecondTimeout(atUsartBuffer.FrameEndTimer, AT_FRAME_END_TIMEOUT)))
    {
        return true;
	}
	else
	{
        return false;
	}
}

// -----------------------------------------------------------------------------
// Zjisti, zda nastala udalost EVENT_AT_RESP_TIMEOUT
// -----------------------------------------------------------------------------
bool Is_AT_RESP_TIMEOUT_Event(void)
{
    if ((atResponseState.Handler != NO_RESPONSE_HANDLER) &&
		(SysTick_IsSysMSecondTimeout(atResponseState.Timer, atResponseState.Timeout)))
	{
        return true;
	}
	else
	{
        return false;
	}
}

// -----------------------------------------------------------------------------
// Zjisti, zda nastala udalost EVENT_WAIT_TIMEOUT
// -----------------------------------------------------------------------------
bool Is_WAIT_TIMEOUT_Event(void)
{
    if ((atWaitState.Handler != NO_WAIT_HANDLER) &&
		(SysTick_IsSysMSecondTimeout(atWaitState.Timer, atWaitState.Timeout)))
	{
        return true;
	}
	else
	{
        return false;
	}
}

// -----------------------------------------------------------------------------
// Vybere udalost
// -----------------------------------------------------------------------------
TEvent PumpEvent(void)
{
    TEvent ev = EVENT_NONE;

	ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
    {
        if ((ev == EVENT_NONE) && Is_WAIT_TIMEOUT_Event())
			ev = EVENT_WAIT_TIMEOUT;

		if ((ev == EVENT_NONE) && Is_AT_RESP_TIMEOUT_Event())
			ev = EVENT_AT_RESP_TIMEOUT;

		if ((ev == EVENT_NONE) && Is_USART_RCV_FRAME_Event())
			ev = EVENT_USART_RCV_FRAME;
	}

	return ev;
}

// -----------------------------------------------------------------------------
// Odesle udalost ke zpracovani
// -----------------------------------------------------------------------------
void DispatchEvent(TEvent event)
{
    switch (event)
	{
        case EVENT_NONE:
			break;

		case EVENT_USART_RCV_FRAME:
            Process_USART_RCV_FRAME_Event();
			break;

		case EVENT_AT_RESP_TIMEOUT:
            Process_AT_RESP_TIMEOUT_Event();
			break;

		case EVENT_WAIT_TIMEOUT:
            Process_WAIT_TIMEOUT_Event();
			break;

		default:
			break;
	}
}

// -----------------------------------------------------------------------------
// Zpracuje udalost EVENT_USART_RCV_FRAME
// -----------------------------------------------------------------------------
void Process_USART_RCV_FRAME_Event(void)
{
    if (atResponseState.Handler != NO_RESPONSE_HANDLER)
	{
        atResponseState.Handler((const char *)atUsartBuffer.Buffer, false);
	}
	else
	{
        CleanAtUsartBuffer();
	}
}

// -----------------------------------------------------------------------------
// Zpracuje udalost EVENT_AT_RESP_TIMEOUT
// -----------------------------------------------------------------------------
void Process_AT_RESP_TIMEOUT_Event(void)
{
    atResponseState.Handler((const char *)atUsartBuffer.Buffer, true);
}

// -----------------------------------------------------------------------------
// Zpracuje udalost EVENT_WAIT_TIMEOUT
// -----------------------------------------------------------------------------
void Process_WAIT_TIMEOUT_Event(void)
{
    atWaitState.Handler();
}

// -----------------------------------------------------------------------------
// inicializace - zavolat jednou pred hlavni smyckou
// -----------------------------------------------------------------------------
void GSM_Init(void)
{
    // Inicializace USART buferu
    InitAtUsartBuffer();

    // init GSM info
    InitGsmInfo();

    // init stavu AT odpovedi
    InitATResponseState();

    // init stavu cekani
    InitATWaitState();

    // cekat po zapnuti modulu
    Wait_AfterReset();
}

// -----------------------------------------------------------------------------
// beh ulohy - musi se volat z hlavni smycky co nejcasteji
// -----------------------------------------------------------------------------
void GSM_Task(void)
{
    if (atUsartBuffer.ByteReceived)
    {
        atUsartBuffer.ByteReceived = false;
        atUsartBuffer.FrameEndTimer = SysTick_GetSysMSeconds();
    }

    DispatchEvent(PumpEvent());
}
